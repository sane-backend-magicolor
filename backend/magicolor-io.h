/*
 * Prototypes for magicolor I/O functions
 *
 * (C) 2010 Reinhold Kainhofer <reinhold@kainhofer.com>
 *
 * Based on the epson2 sane backend:
 * Based on Kazuhiro Sasayama previous
 * Work on epson.[ch] file from the SANE package.
 * Please see those files for original copyrights.
 * Copyright (C) 2006 Tower Technologies
 * Author: Alessandro Zummo <a.zummo@towertech.it>
 *
 * This file is part of the SANE package.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, version 2.
 */

#ifndef magicolor_io_h
#define magicolor_io_h

/* Debug output: Print hex representation of buffer */
void dump_hex_buffer_dense (int level, const unsigned char *buf, size_t buf_size);

/* Create buffers containing the command and arguments. Length of reserved
 * buffer is returned. It's the caller's job to free the buffer! */
int mc_create_buffer (Magicolor_Scanner *s, unsigned char cmd_type, unsigned char cmd,
		      unsigned char **buf, unsigned char* arg1, size_t len1, SANE_Status *status);
int mc_create_buffer2 (Magicolor_Scanner *s, unsigned char cmd_type, unsigned char cmd,
		       unsigned char **buf, unsigned char* arg1, size_t len1,
		       unsigned char* arg2, size_t len2, SANE_Status *status);


int mc_send(Magicolor_Scanner * s, void *buf, size_t buf_size, SANE_Status * status);
ssize_t mc_recv(Magicolor_Scanner * s, void *buf, ssize_t buf_size, SANE_Status * status);

SANE_Status
mc_txrx(Magicolor_Scanner * s, unsigned char *txbuf, size_t txlen,
	    unsigned char *rxbuf, size_t rxlen);

#endif /* magicolor_io_h */
