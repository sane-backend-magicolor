/*
 * magicolor.c - SANE library for Magicolor scanners.
 *
 * (C) 2010 Reinhold Kainhofer <reinhold@kainhofer.com>
 *
 * Based on the epson2 sane backend:
 * Based on Kazuhiro Sasayama previous
 * Work on epson.[ch] file from the SANE package.
 * Please see those files for additional copyrights.
 * Copyright (C) 2006-10 Tower Technologies
 * Author: Alessandro Zummo <a.zummo@towertech.it>
 *
 * This file is part of the SANE package.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, version 2.
 */

#define MAGICOLOR_VERSION	0
#define MAGICOLOR_REVISION	0
#define MAGICOLOR_BUILD	1

/* debugging levels:
 *
 *     127	mc_recv buffer
 *     125	mc_send buffer
 *	35	fine-grained status and progress
 *	30	sane_read
 *	25	setvalue, getvalue, control_option
 *	20	low-level (I/O) mc_* functions
 *	15	mid-level mc_* functions
 *	10	high-level cmd_* functions
 *	 7	open/close/attach
 *	 6	print_params
 *	 5	basic functions
 *	 3	status info and progress
 *	 2	scanner info and capabilities
 *	 1	errors & warnings
 */

#include "sane/config.h"

#include "magicolor.h"

#include <limits.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <sys/time.h>

#if HAVE_LIBSNMP
#include <net-snmp/net-snmp-config.h>
#include <net-snmp/net-snmp-includes.h>
#include <net-snmp/library/snmp_transport.h>
#include <arpa/inet.h>
#endif

#include "sane/saneopts.h"
#include "sane/sanei_usb.h"
#include "sane/sanei_tcp.h"
#include "sane/sanei_udp.h"
#include "sane/sanei_backend.h"
#include "sane/sanei_config.h"

#include "magicolor-io.h"
#include "magicolor-commands.h"
#include "magicolor-ops.h"

#include "magicolor_usb.h"
#include "magicolor_net.h"

/*
 * Definition of the mode_param struct, that is used to
 * specify the valid parameters for the different scan modes.
 *
 * The depth variable gets updated when the bit depth is modified.
 */

struct mode_param mode_params[] = {
	{0x00, 1, 1},  /* Lineart, 1 color, 1 bit */
	{0x02, 1, 24}, /* Grayscale, 1 color, 24 bit */
	{0x03, 3, 24}  /* Color, 3 colors, 24 bit */
};

static SANE_String_Const mode_list[] = {
	SANE_VALUE_SCAN_MODE_LINEART,
	SANE_VALUE_SCAN_MODE_GRAY,
	SANE_VALUE_SCAN_MODE_COLOR,
	NULL
};

static const SANE_String_Const adf_mode_list[] = {
	SANE_I18N("Simplex"),
	SANE_I18N("Duplex"),
	NULL
};

/* Define the different scan sources */

#define FBF_STR	SANE_I18N("Flatbed")
#define ADF_STR	SANE_I18N("Automatic Document Feeder")

/*
 * source list need one dummy entry (save device settings is crashing).
 * NOTE: no const - this list gets created while exploring the capabilities
 * of the scanner.
 */

SANE_String_Const source_list[] = {
	FBF_STR,
	NULL,
	NULL,
	NULL
};

/* minimum, maximum, quantization */
static const SANE_Range u8_range = { 0, 255, 0 };
static const SANE_Range s8_range = { -127, 127, 0 };

static const SANE_Range outline_emphasis_range = { -2, 2, 0 };

/* Use one second for SNMP discovery */
#define MC_AutoDetectionTimeout 1

/*
 * List of pointers to devices - will be dynamically allocated depending
 * on the number of devices found.
 */
static const SANE_Device **devlist;


/* Some utility functions */

static size_t
max_string_size(const SANE_String_Const strings[])
{
	size_t size, max_size = 0;
	int i;

	for (i = 0; strings[i]; i++) {
		size = strlen(strings[i]) + 1;
		if (size > max_size)
			max_size = size;
	}
	return max_size;
}

static SANE_Status attach_one_usb(SANE_String_Const devname);
static SANE_Status attach_one_net(SANE_String_Const devname, unsigned int device);

static void
print_params(const SANE_Parameters params)
{
	DBG(6, "params.format          = %d\n", params.format);
	DBG(6, "params.last_frame      = %d\n", params.last_frame);
	DBG(6, "params.bytes_per_line  = %d\n", params.bytes_per_line);
	DBG(6, "params.pixels_per_line = %d\n", params.pixels_per_line);
	DBG(6, "params.lines           = %d\n", params.lines);
	DBG(6, "params.depth           = %d\n", params.depth);
}


/*
 * close_scanner()
 *
 * Close the open scanner. Depending on the connection method, a different
 * close function is called.
 */

static void
close_scanner(Magicolor_Scanner *s)
{
	DBG(7, "%s: fd = %d\n", __func__, s->fd);

	if (s->fd == -1)
		return;

	mc_scan_finish(s);
	if (s->hw->connection == SANE_MAGICOLOR_NET) {
		sanei_magicolor_net_close(s);
		sanei_tcp_close(s->fd);
	} else if (s->hw->connection == SANE_MAGICOLOR_USB) {
		sanei_usb_close(s->fd);
	}

	s->fd = -1;
}

static SANE_Bool
split_scanner_name (const char *name, char * IP, unsigned int *model);

static SANE_Bool
split_scanner_name (const char *name, char * IP, unsigned int *model)
{
	const char *device = name;
	const char *qm;
	*model = 0;
	/* cut off leading net: */
	if (strncmp(device, "net:", 4) == 0)
		device = &device[4];

	qm = strchr(device, '?');
	if (qm != NULL) {
		size_t len = qm-device;
		strncpy (IP, device, len);
		IP[len] = '\0';
		qm++;
		if (strncmp(qm, "model=", 6) == 0) {
			qm += 6;
			if (!sscanf(qm, "0x%x", model))
				sscanf(qm, "%x", model);
		}
	} else {
		strcpy (IP, device);
	}
	return SANE_TRUE;
}

/*
 * open_scanner()
 *
 * Open the scanner device. Depending on the connection method,
 * different open functions are called.
 */

static SANE_Status
open_scanner(Magicolor_Scanner *s)
{
	SANE_Status status = 0;

	DBG(7, "%s: %s\n", __func__, s->hw->sane.name);

	if (s->fd != -1) {
		DBG(7, "scanner is already open: fd = %d\n", s->fd);
		return SANE_STATUS_GOOD;	/* no need to open the scanner */
	}

	if (s->hw->connection == SANE_MAGICOLOR_NET) {
		/* device name has the form net:ipaddr?model=... */
		char IP[1024];
		unsigned int model = 0;
		if (!split_scanner_name (s->hw->sane.name, IP, &model))
			return SANE_STATUS_INVAL;
		status = sanei_tcp_open(IP, 4567, &s->fd);
		if (model>0)
			mc_set_device (s, model);
		if (status == SANE_STATUS_GOOD) {
			DBG(7, "awaiting welcome message\n");
			status = sanei_magicolor_net_open (s);
		}

	} else if (s->hw->connection == SANE_MAGICOLOR_USB)
		status = sanei_usb_open(s->hw->sane.name, &s->fd);

	if (status == SANE_STATUS_ACCESS_DENIED) {
		DBG(1, "please check that you have permissions on the device.\n");
		DBG(1, "if this is a multi-function device with a printer,\n");
		DBG(1, "disable any conflicting driver (like usblp).\n");
	}

	if (status != SANE_STATUS_GOOD)
		DBG(1, "%s open failed: %s\n", s->hw->sane.name,
			sane_strstatus(status));
	else
		DBG(3, "scanner opened\n");

	return status;
}

static SANE_Status
detect_usb(struct Magicolor_Scanner *s)
{
	SANE_Status status;
	int vendor, product;
	int i, numIds;
	SANE_Bool is_valid;

	/* if the sanei_usb_get_vendor_product call is not supported,
	 * then we just ignore this and rely on the user to config
	 * the correct device.
	 */

	status = sanei_usb_get_vendor_product(s->fd, &vendor, &product);
	if (status != SANE_STATUS_GOOD) {
		DBG(1, "the device cannot be verified - will continue\n");
		return SANE_STATUS_GOOD;
	}

	/* check the vendor ID to see if we are dealing with an MAGICOLOR device */
	if (vendor != SANE_MAGICOLOR_VENDOR_ID) {
		/* this is not a supported vendor ID */
		DBG(1, "not an Magicolor device at %s (vendor id=0x%x)\n",
		    s->hw->sane.name, vendor);
		return SANE_STATUS_INVAL;
	}

	numIds = sanei_magicolor_getNumberOfUSBProductIds();
	is_valid = SANE_FALSE;
	i = 0;

	/* check all known product IDs to verify that we know
	 *	   about the device */
	while (i != numIds && !is_valid) {
		if (product == sanei_magicolor_usb_product_ids[i])
			is_valid = SANE_TRUE;
		i++;
	}

	if (is_valid == SANE_FALSE) {
		DBG(1, "the device at %s is not a supported (product id=0x%x)\n",
		    s->hw->sane.name, product);
		return SANE_STATUS_INVAL;
	}

	DBG(2, "found valid Magicolor scanner: 0x%x/0x%x (vendorID/productID)\n",
	    vendor, product);

	return SANE_STATUS_GOOD;
}

static int num_devices;		/* number of scanners attached to backend */
static Magicolor_Device *first_dev;	/* first MAGICOLOR scanner in list */

static struct Magicolor_Scanner *
scanner_create(struct Magicolor_Device *dev, SANE_Status *status)
{
	struct Magicolor_Scanner *s;

	s = malloc(sizeof(struct Magicolor_Scanner));
	if (s == NULL) {
		*status = SANE_STATUS_NO_MEM;
		return NULL;
	}

	memset(s, 0x00, sizeof(struct Magicolor_Scanner));

	s->fd = -1;
	s->hw = dev;

	return s;
}

static struct Magicolor_Scanner *
device_detect(const char *name, int type, SANE_Status *status)
{
	/* TODO: Do we need to reset the scanner at any point???? Was in the epson2 backend, removed here... */
	struct Magicolor_Scanner *s;
	struct Magicolor_Device *dev;

	/* try to find the device in our list */
	for (dev = first_dev; dev; dev = dev->next) {
		if (strcmp(dev->sane.name, name) == 0) {

			/* the device might have been just probed,
			 * sleep a bit.
			 */
			if (dev->connection == SANE_MAGICOLOR_NET)
				sleep(1);

			return scanner_create(dev, status);
		}
	}

	if (type == SANE_MAGICOLOR_NODEV) {
		*status = SANE_STATUS_INVAL;
		return NULL;
	}

	/* alloc and clear our device structure */
	dev = malloc(sizeof(*dev));
	if (!dev) {
		*status = SANE_STATUS_NO_MEM;
		return NULL;
	}
	memset(dev, 0x00, sizeof(struct Magicolor_Device));

	s = scanner_create(dev, status);
	if (s == NULL)
		return NULL;

	mc_dev_init(dev, name, type);

	*status = open_scanner(s);
	if (*status != SANE_STATUS_GOOD) {
		free(s);
		return NULL;
	}

	/* from now on, close_scanner() must be called */

	/* USB requires special care */
	if (dev->connection == SANE_MAGICOLOR_USB) {
		*status = detect_usb(s);
	}

	if (*status != SANE_STATUS_GOOD)
		goto close;

	/* set name and model (if not already set) */
	if (dev->model == NULL)
		mc_set_model(s, "generic", 7);

	dev->name = strdup(name);
	dev->sane.name = dev->name;

	*status = mc_discover_capabilities(s);
	if (*status != SANE_STATUS_GOOD)
		goto close;

	if (source_list[0] == NULL || dev->cap->dpi_range.min == 0) {
		DBG(1, "something is wrong in the discovery process, aborting.\n");
		*status = SANE_STATUS_IO_ERROR;
		goto close;
	}

	mc_dev_post_init(dev);

	/* add this scanner to the device list */
	num_devices++;
	dev->next = first_dev;
	first_dev = dev;

	return s;

	close:
	close_scanner(s);
	free(s);
	return NULL;
}

#if HAVE_LIBSNMP
/** Handle one SNMP response (whether received sync or async) and if describes
 * a magicolor device, attach it. Returns the number of attached devices (0
 * or 1) */
static int
mc_network_discovery_handle (struct snmp_pdu *pdu)
{
	netsnmp_variable_list *varlist = pdu->variables, *vp;
	oid anOID[MAX_OID_LEN];
	size_t anOID_len = MAX_OID_LEN;
	/* Device information variables */
	char ip_addr[1024];
	char model[1024];
	char device[1024];
	/* remote IP detection variables */
	netsnmp_indexed_addr_pair *responder = (netsnmp_indexed_addr_pair *) pdu->transport_data;
	struct sockaddr_in *remote = NULL;
	struct MagicolorCap *cap;

	DBG(5, "%s: Handling SNMP response \n", __func__);

	if (responder == NULL || pdu->transport_data_length != sizeof(netsnmp_indexed_addr_pair )) {
		DBG(1, "%s: Unable to extract IP address from SNMP response.\n",
		    __func__);
		return 0;
	}
	remote = (struct sockaddr_in *) &(responder->remote_addr);
	if (remote == NULL) {
		DBG(1, "%s: Unable to extract IP address from SNMP response.\n",
			__func__);
		return 0;
	}
	snprintf(ip_addr, sizeof(ip_addr), "%s", inet_ntoa(remote->sin_addr));
	DBG(35, "%s: IP Address of responder is %s\n", __func__, ip_addr);

	/* System Object ID (Unique OID identifying model)
	 * This determines whether we really have a magicolor device */
	anOID_len = MAX_OID_LEN;
	read_objid(MAGICOLOR_SNMP_SYSOBJECT_OID, anOID, &anOID_len);
	vp = find_varbind_in_list (varlist, anOID, anOID_len);
	if (vp) {
		size_t value_len = vp->val_len/sizeof(oid);
		if (vp->type != ASN_OBJECT_ID) {
			DBG (3, "%s: SystemObjectID does not return an OID, device is not a magicolor device\n", __func__);
			return 0;
		}
		snprint_objid (device, sizeof(device), vp->val.objid, value_len);
		DBG (5, "%s: Device object ID is '%s'\n", __func__, device);

		anOID_len = MAX_OID_LEN;
		read_objid (MAGICOLOR_SNMP_DEVICE_TREE, anOID, &anOID_len);
		if (netsnmp_oid_is_subtree (anOID, anOID_len,
				vp->val.objid, value_len) == 0) {
			DBG (5, "%s: Device appears to be a magicolor device (OID=%s)\n", __func__, device);
		} else {
			DBG (5, "%s: Device is not a Magicolor device\n", __func__);
			return 0;
		}
	}

	/* Retrieve sysDescr (i.e. model name) */
	anOID_len = MAX_OID_LEN;
	read_objid(MAGICOLOR_SNMP_SYSDESCR_OID, anOID, &anOID_len);
	vp = find_varbind_in_list (varlist, anOID, anOID_len);
	if (vp) {
		memcpy(model,vp->val.string,vp->val_len);
		model[vp->val_len] = '\0';
		DBG (5, "%s: Found model: %s\n", __func__, model);
	}

	DBG (1, "%s: Detected device '%s' on IP %s\n",
	     __func__, model, ip_addr);

	vp = pdu->variables;
	/* TODO: attach the IP with attach_one_net(ip) */
	cap = mc_get_device_from_identification (device);
	if (cap) {
		DBG(1, "%s: Found autodiscovered device: %s (type 0x%x)\n", __func__, cap->model, cap->id);
		attach_one_net (ip_addr, cap->id);
		return 1;
	}
	return 0;
}

static int
mc_network_discovery_cb (int operation, struct snmp_session *sp, int reqid,
			 struct snmp_pdu *pdu, void *magic)
{
	NOT_USED (reqid);
	NOT_USED (sp);
	DBG(5, "%s: Received broadcast response \n", __func__);

	if (operation == NETSNMP_CALLBACK_OP_RECEIVED_MESSAGE) {
		int nr = mc_network_discovery_handle (pdu);
		int *m = (int*)magic;
		*m += nr;
		DBG(5, "%s: Added %d discovered host(s) for SNMP response.\n", __func__, nr);
	}

	return 1;
}
#endif

/* Use SNMP for automatic network discovery. If host is given, try to detect
 * that one host (using sync SNMP, otherwise send an SNMP broadcast (async).
 */
static int
mc_network_discovery(const char*host)
{
#if HAVE_LIBSNMP
	netsnmp_session session, *ss;
	netsnmp_pdu *pdu;
	oid anOID[MAX_OID_LEN];
	size_t anOID_len = MAX_OID_LEN;
	time_t endtime; /* end time for SNMP scan */
	int nr = 0; /* Nr of added hosts */


	DBG(1, "%s: running network discovery \n", __func__);

	/* Win32: init winsock */
	SOCK_STARTUP;
	init_snmp("sane-magicolor-backend");
	snmp_sess_init (&session);
	session.version = SNMP_VERSION_2c;
	session.community = "public";
	session.community_len = strlen (session.community);
	if (host) {
		session.peername = host;
	} else {
		/* Do a network discovery via a broadcast */
		session.peername = "255.255.255.255";
		session.flags   |= SNMP_FLAGS_UDP_BROADCAST;
		session.callback = mc_network_discovery_cb;
		session.callback_magic = &nr;
	}

	ss = snmp_open (&session); /* establish the session */
	if (!ss) {
		snmp_sess_perror ("ack", &session);
		SOCK_CLEANUP;
		return 0;
	}

	/* Create the PDU for the data for our request and add the three
	 * desired OIDs to the PDU */
	pdu = snmp_pdu_create (SNMP_MSG_GET);

	/* SNMPv2-MIB::sysDescr.0 */
	anOID_len = MAX_OID_LEN;
	if (read_objid(MAGICOLOR_SNMP_SYSDESCR_OID, anOID, &anOID_len)) {
		snmp_add_null_var (pdu, anOID, anOID_len);
	}
	/* SNMPv2-MIB::sysObjectID.0 */
	anOID_len = MAX_OID_LEN;
	if (read_objid(MAGICOLOR_SNMP_SYSOBJECT_OID, anOID, &anOID_len)) {
		snmp_add_null_var (pdu, anOID, anOID_len);
	}
	/* IF-MIB::ifPhysAddress.1 */
	anOID_len = MAX_OID_LEN;
	if (read_objid(MAGICOLOR_SNMP_MAC_OID, anOID, &anOID_len)) {
		snmp_add_null_var (pdu, anOID, anOID_len);
	}
	/* TODO: Add more interesting OIDs, in particular vendor OIDs */

	/* Now send out the request and wait for responses for some time.
	 * If we get a response, connect to that device (in the callback),
	 * otherwise we probably don't have a magicolor device in the
	 * LAN (or SNMP is turned off, in which case we have no way to detect
	 * it.
	 */
	DBG(100, "%s: Sending SNMP packet\n", __func__);
	if (host) {
		/* sync request, immediately read the reply */
		netsnmp_pdu *response = 0;
		int status = snmp_synch_response(ss, pdu, &response);
 		if (status == STAT_SUCCESS && response->errstat == SNMP_ERR_NOERROR) {
			nr = mc_network_discovery_handle (response);
		}
		if (response)
			snmp_free_pdu(response);


	} else {
		if (!snmp_send(ss, pdu)) {
			snmp_free_pdu(pdu);
			DBG(100, "%s: Sending SNMP packet NOT successful\n", __func__);
			return 0;
		}
		/* listen for responses for MC_AutoDetectionTimeout seconds: */
		endtime = time(NULL) + MC_AutoDetectionTimeout;

		while (time(NULL) < endtime) {
			int fds = 0, block = 0;
			fd_set fdset;
			struct timeval timeout; /* timeout for select() */
			timeout.tv_sec = 0;
			timeout.tv_usec = 5000;
			FD_ZERO (&fdset);
			snmp_select_info (&fds, &fdset, &timeout, &block);
			fds = select (fds, &fdset, NULL, NULL, /*block?NULL:*/&timeout);
			if (fds) snmp_read(&fdset);
			else snmp_timeout();
		}
	}

	/* Clean up */
	snmp_close(ss);
	SOCK_CLEANUP;
	DBG (5, "%s: Discovered %d host(s)\n", __func__, nr);
	return nr;

#else
	DBG (1, "%s: net-snmp library not enabled, auto-detecting network scanners not supported.\n", __func__);
	return 0;
#endif
}

static SANE_Status
attach(const char *name, int type)
{
	SANE_Status status;
	Magicolor_Scanner *s;

	DBG(7, "%s: devname = %s, type = %d\n", __func__, name, type);

	s = device_detect(name, type, &status);
	if(s == NULL)
		return status;

	close_scanner(s);
	free(s);
	return status;
}

SANE_Status
attach_one_usb(const char *dev)
{
	DBG(7, "%s: dev = %s\n", __func__, dev);
	return attach(dev, SANE_MAGICOLOR_USB);
}

static SANE_Status
attach_one_net(const char *dev, unsigned int model)
{
	char name[1024];

	DBG(7, "%s: dev = %s\n", __func__, dev);
	if (model > 0) {
		snprintf(name, 1024, "net:%s?model=0x%x", dev, model);
	} else {
		snprintf(name, 1024, "net:%s", dev);
	}

	return attach(name, SANE_MAGICOLOR_NET);
}

static SANE_Status
attach_one_config(SANEI_Config __sane_unused__ *config, const char *line)
{
	int vendor, product;

	int len = strlen(line);

	DBG(7, "%s: len = %d, line = %s\n", __func__, len, line);

	if (sscanf(line, "usb %i %i", &vendor, &product) == 2) {
		/* add the vendor and product IDs to the list of
		 *		   known devices before we call the attach function */

		int numIds = sanei_magicolor_getNumberOfUSBProductIds();

		if (vendor != SANE_MAGICOLOR_VENDOR_ID)
			return SANE_STATUS_INVAL; /* this is not a KONICA MINOLTA device */

		sanei_magicolor_usb_product_ids[numIds - 1] = product;
		sanei_usb_attach_matching_devices(line, attach_one_usb);

	} else if (strncmp(line, "usb", 3) == 0 && len == 3) {
		int i, numIds;

		numIds = sanei_magicolor_getNumberOfUSBProductIds();

		for (i = 0; i < numIds; i++) {
			sanei_usb_find_devices(SANE_MAGICOLOR_VENDOR_ID,
					       sanei_magicolor_usb_product_ids[i], attach_one_usb);
		}

	} else if (strncmp(line, "net", 3) == 0) {

		/* remove the "net" sub string */
		const char *name = sanei_config_skip_whitespace(line + 3);
		char IP[1024];
		unsigned int model = 0;

		if (strncmp(name, "autodiscovery", 13) == 0) {
			DBG (50, "%s: Initiating network autodiscovervy via SNMP\n", __func__);
			mc_network_discovery(NULL);
		} else if (sscanf(name, "%s %x", IP, &model) == 2) {
			DBG(50, "%s: Using network device on IP %s, forcing model 0x%x\n", __func__, IP, model);
			attach_one_net(IP, model);
		} else {
			/* use SNMP to detect the type. If not successful,
			 * add the host with model type 0 */
			DBG(50, "%s: Using network device on IP %s, trying to autodetect model\n", __func__, IP);
			if (mc_network_discovery(name)==0) {
				DBG(1, "%s: Autodetecting device model failed, using default model\n", __func__, IP);
				attach_one_net(name, 0);
			}
		}

	} else {
		/* TODO! */
	}

	return SANE_STATUS_GOOD;
}

static void
free_devices(void)
{
	Magicolor_Device *dev, *next;

	DBG(5, "%s\n", __func__);

	for (dev = first_dev; dev; dev = next) {
		next = dev->next;
		free(dev->name);
		free(dev->model);
		free(dev);
	}

	free(devlist);

	first_dev = NULL;
}

static void
probe_devices(void)
{
	const char *dir_list;
	DBG(5, "%s\n", __func__);
	dir_list =  getenv ("SANE_CONFIG_DIR");
	DBG (127, "SANE_CONFIG_DIR: %s\n", dir_list);

	free_devices();
	sanei_configure_attach(MAGICOLOR_CONFIG_FILE, NULL,
			       attach_one_config);
}

SANE_Status
sane_init(SANE_Int *version_code, SANE_Auth_Callback __sane_unused__ authorize)
{
	DBG_INIT();
	DBG(2, "%s: " PACKAGE " " VERSION "\n", __func__);

	DBG(1, "magicolor backend, version %i.%i.%i\n",
	    MAGICOLOR_VERSION, MAGICOLOR_REVISION, MAGICOLOR_BUILD);

	if (version_code != NULL)
		*version_code = SANE_VERSION_CODE(SANE_CURRENT_MAJOR, V_MINOR,
						  MAGICOLOR_BUILD);

		sanei_usb_init();

	return SANE_STATUS_GOOD;
}

/* Clean up the list of attached scanners. */
void
sane_exit(void)
{
	DBG(5, "%s\n", __func__);
	free_devices();
}

SANE_Status
sane_get_devices(const SANE_Device ***device_list, SANE_Bool __sane_unused__ local_only)
{
	Magicolor_Device *dev;
	int i;

	DBG(5, "%s\n", __func__);

	probe_devices();

	devlist = malloc((num_devices + 1) * sizeof(devlist[0]));
	if (!devlist) {
		DBG(1, "out of memory (line %d)\n", __LINE__);
		return SANE_STATUS_NO_MEM;
	}

	DBG(5, "%s - results:\n", __func__);

	for (i = 0, dev = first_dev; i < num_devices && dev; dev = dev->next, i++) {
		DBG(1, " %d (%d): %s\n", i, dev->connection, dev->model);
		devlist[i] = &dev->sane;
	}

	devlist[i] = NULL;

	*device_list = devlist;

	return SANE_STATUS_GOOD;
}

static SANE_Status
init_options(Magicolor_Scanner *s)
{
	int i;
	SANE_Word *res_list;

	for (i = 0; i < NUM_OPTIONS; i++) {
		s->opt[i].size = sizeof(SANE_Word);
		s->opt[i].cap = SANE_CAP_SOFT_SELECT | SANE_CAP_SOFT_DETECT;
	}

	s->opt[OPT_NUM_OPTS].title = SANE_TITLE_NUM_OPTIONS;
	s->opt[OPT_NUM_OPTS].desc = SANE_DESC_NUM_OPTIONS;
	s->opt[OPT_NUM_OPTS].type = SANE_TYPE_INT;
	s->opt[OPT_NUM_OPTS].cap = SANE_CAP_SOFT_DETECT;
	s->val[OPT_NUM_OPTS].w = NUM_OPTIONS;

	/* "Scan Mode" group: */

	s->opt[OPT_MODE_GROUP].title = SANE_I18N("Scan Mode");
	s->opt[OPT_MODE_GROUP].desc = "";
	s->opt[OPT_MODE_GROUP].type = SANE_TYPE_GROUP;
	s->opt[OPT_MODE_GROUP].cap = 0;

	/* scan mode */
	s->opt[OPT_MODE].name = SANE_NAME_SCAN_MODE;
	s->opt[OPT_MODE].title = SANE_TITLE_SCAN_MODE;
	s->opt[OPT_MODE].desc = SANE_DESC_SCAN_MODE;
	s->opt[OPT_MODE].type = SANE_TYPE_STRING;
	s->opt[OPT_MODE].size = max_string_size(mode_list);
	s->opt[OPT_MODE].constraint_type = SANE_CONSTRAINT_STRING_LIST;
	s->opt[OPT_MODE].constraint.string_list = mode_list;
	s->val[OPT_MODE].w = 0;	/* Binary */

	/* bit depth */
	s->opt[OPT_BIT_DEPTH].name = SANE_NAME_BIT_DEPTH;
	s->opt[OPT_BIT_DEPTH].title = SANE_TITLE_BIT_DEPTH;
	s->opt[OPT_BIT_DEPTH].desc = SANE_DESC_BIT_DEPTH;
	s->opt[OPT_BIT_DEPTH].type = SANE_TYPE_INT;
	s->opt[OPT_BIT_DEPTH].unit = SANE_UNIT_NONE;
	s->opt[OPT_BIT_DEPTH].constraint_type = SANE_CONSTRAINT_WORD_LIST;
	s->opt[OPT_BIT_DEPTH].constraint.word_list = s->hw->cap->depth_list;
	s->opt[OPT_BIT_DEPTH].cap |= SANE_CAP_INACTIVE;
	s->val[OPT_BIT_DEPTH].w = s->hw->cap->depth_list[1];	/* the first "real" element is the default */

	if (s->hw->cap->depth_list[0] == 1)	/* only one element in the list -> hide the option */
		s->opt[OPT_BIT_DEPTH].cap |= SANE_CAP_INACTIVE;




	/* brightness */
	s->opt[OPT_BRIGHTNESS].name = SANE_NAME_BRIGHTNESS;
	s->opt[OPT_BRIGHTNESS].title = SANE_TITLE_BRIGHTNESS;
	s->opt[OPT_BRIGHTNESS].desc = SANE_I18N("Selects the brightness.");
	s->opt[OPT_BRIGHTNESS].type = SANE_TYPE_INT;
	s->opt[OPT_BRIGHTNESS].unit = SANE_UNIT_NONE;
	s->opt[OPT_BRIGHTNESS].constraint_type = SANE_CONSTRAINT_RANGE;
	s->opt[OPT_BRIGHTNESS].constraint.range = &s->hw->cap->brightness;
	s->val[OPT_BRIGHTNESS].w = 5;	/* Normal */

	/* resolution */
	s->opt[OPT_RESOLUTION].name = SANE_NAME_SCAN_RESOLUTION;
	s->opt[OPT_RESOLUTION].title = SANE_TITLE_SCAN_RESOLUTION;
	s->opt[OPT_RESOLUTION].desc = SANE_DESC_SCAN_RESOLUTION;
	s->opt[OPT_RESOLUTION].type = SANE_TYPE_INT;
	s->opt[OPT_RESOLUTION].unit = SANE_UNIT_DPI;
	s->opt[OPT_RESOLUTION].constraint_type = SANE_CONSTRAINT_WORD_LIST;
	res_list = malloc((s->hw->cap->res_list_size + 1) * sizeof(SANE_Word));
	if (res_list == NULL) {
		return SANE_STATUS_NO_MEM;
	}
	*(res_list) = s->hw->cap->res_list_size;
	memcpy(&(res_list[1]), s->hw->cap->res_list, s->hw->cap->res_list_size * sizeof(SANE_Word));
	s->opt[OPT_RESOLUTION].constraint.word_list = res_list;
	s->val[OPT_RESOLUTION].w = s->hw->cap->dpi_range.min;


	/* "Advanced" group: */
	s->opt[OPT_ADVANCED_GROUP].title = SANE_I18N("Advanced");
	s->opt[OPT_ADVANCED_GROUP].desc = "";
	s->opt[OPT_ADVANCED_GROUP].type = SANE_TYPE_GROUP;
	s->opt[OPT_ADVANCED_GROUP].cap = SANE_CAP_ADVANCED;

	/* "Preview settings" group: */
	s->opt[OPT_PREVIEW_GROUP].title = SANE_TITLE_PREVIEW;
	s->opt[OPT_PREVIEW_GROUP].desc = "";
	s->opt[OPT_PREVIEW_GROUP].type = SANE_TYPE_GROUP;
	s->opt[OPT_PREVIEW_GROUP].cap = SANE_CAP_ADVANCED;

	/* preview */
	s->opt[OPT_PREVIEW].name = SANE_NAME_PREVIEW;
	s->opt[OPT_PREVIEW].title = SANE_TITLE_PREVIEW;
	s->opt[OPT_PREVIEW].desc = SANE_DESC_PREVIEW;
	s->opt[OPT_PREVIEW].type = SANE_TYPE_BOOL;
	s->val[OPT_PREVIEW].w = SANE_FALSE;

	/* "Geometry" group: */
	s->opt[OPT_GEOMETRY_GROUP].title = SANE_I18N("Geometry");
	s->opt[OPT_GEOMETRY_GROUP].desc = "";
	s->opt[OPT_GEOMETRY_GROUP].type = SANE_TYPE_GROUP;
	s->opt[OPT_GEOMETRY_GROUP].cap = SANE_CAP_ADVANCED;

	/* top-left x */
	s->opt[OPT_TL_X].name = SANE_NAME_SCAN_TL_X;
	s->opt[OPT_TL_X].title = SANE_TITLE_SCAN_TL_X;
	s->opt[OPT_TL_X].desc = SANE_DESC_SCAN_TL_X;
	s->opt[OPT_TL_X].type = SANE_TYPE_FIXED;
	s->opt[OPT_TL_X].unit = SANE_UNIT_MM;
	s->opt[OPT_TL_X].constraint_type = SANE_CONSTRAINT_RANGE;
	s->opt[OPT_TL_X].constraint.range = s->hw->x_range;
	s->val[OPT_TL_X].w = 0;

	/* top-left y */
	s->opt[OPT_TL_Y].name = SANE_NAME_SCAN_TL_Y;
	s->opt[OPT_TL_Y].title = SANE_TITLE_SCAN_TL_Y;
	s->opt[OPT_TL_Y].desc = SANE_DESC_SCAN_TL_Y;
	s->opt[OPT_TL_Y].type = SANE_TYPE_FIXED;
	s->opt[OPT_TL_Y].unit = SANE_UNIT_MM;
	s->opt[OPT_TL_Y].constraint_type = SANE_CONSTRAINT_RANGE;
	s->opt[OPT_TL_Y].constraint.range = s->hw->y_range;
	s->val[OPT_TL_Y].w = 0;

	/* bottom-right x */
	s->opt[OPT_BR_X].name = SANE_NAME_SCAN_BR_X;
	s->opt[OPT_BR_X].title = SANE_TITLE_SCAN_BR_X;
	s->opt[OPT_BR_X].desc = SANE_DESC_SCAN_BR_X;
	s->opt[OPT_BR_X].type = SANE_TYPE_FIXED;
	s->opt[OPT_BR_X].unit = SANE_UNIT_MM;
	s->opt[OPT_BR_X].constraint_type = SANE_CONSTRAINT_RANGE;
	s->opt[OPT_BR_X].constraint.range = s->hw->x_range;
	s->val[OPT_BR_X].w = s->hw->x_range->max;

	/* bottom-right y */
	s->opt[OPT_BR_Y].name = SANE_NAME_SCAN_BR_Y;
	s->opt[OPT_BR_Y].title = SANE_TITLE_SCAN_BR_Y;
	s->opt[OPT_BR_Y].desc = SANE_DESC_SCAN_BR_Y;
	s->opt[OPT_BR_Y].type = SANE_TYPE_FIXED;
	s->opt[OPT_BR_Y].unit = SANE_UNIT_MM;
	s->opt[OPT_BR_Y].constraint_type = SANE_CONSTRAINT_RANGE;
	s->opt[OPT_BR_Y].constraint.range = s->hw->y_range;
	s->val[OPT_BR_Y].w = s->hw->y_range->max;

	/* "Optional equipment" group: */
	s->opt[OPT_EQU_GROUP].title = SANE_I18N("Optional equipment");
	s->opt[OPT_EQU_GROUP].desc = "";
	s->opt[OPT_EQU_GROUP].type = SANE_TYPE_GROUP;
	s->opt[OPT_EQU_GROUP].cap = SANE_CAP_ADVANCED;

	/* source */
	s->opt[OPT_SOURCE].name = SANE_NAME_SCAN_SOURCE;
	s->opt[OPT_SOURCE].title = SANE_TITLE_SCAN_SOURCE;
	s->opt[OPT_SOURCE].desc = SANE_DESC_SCAN_SOURCE;
	s->opt[OPT_SOURCE].type = SANE_TYPE_STRING;
	s->opt[OPT_SOURCE].size = max_string_size(source_list);
	s->opt[OPT_SOURCE].constraint_type = SANE_CONSTRAINT_STRING_LIST;
	s->opt[OPT_SOURCE].constraint.string_list = source_list;
	s->val[OPT_SOURCE].w = 0;	/* always use Flatbed as default */

	s->opt[OPT_ADF_MODE].name = "adf-mode";
	s->opt[OPT_ADF_MODE].title = SANE_I18N("ADF Mode");
	s->opt[OPT_ADF_MODE].desc =
		SANE_I18N("Selects the ADF mode (simplex/duplex)");
	s->opt[OPT_ADF_MODE].type = SANE_TYPE_STRING;
	s->opt[OPT_ADF_MODE].size = max_string_size(adf_mode_list);
	s->opt[OPT_ADF_MODE].constraint_type = SANE_CONSTRAINT_STRING_LIST;
	s->opt[OPT_ADF_MODE].constraint.string_list = adf_mode_list;
	s->val[OPT_ADF_MODE].w = 0;	/* simplex */
	if ((!s->hw->cap->ADF) || (s->hw->cap->adf_duplex == SANE_FALSE))
		s->opt[OPT_ADF_MODE].cap |= SANE_CAP_INACTIVE;

	return SANE_STATUS_GOOD;
}

SANE_Status
sane_open(SANE_String_Const name, SANE_Handle *handle)
{
	SANE_Status status;
	Magicolor_Scanner *s = NULL;

	int l = strlen(name);

	DBG(7, "%s: name = %s\n", __func__, name);

	/* probe if empty device name provided */
	if (l == 0) {

		probe_devices();

		if (first_dev == NULL) {
			DBG(1, "no device detected\n");
			return SANE_STATUS_INVAL;
		}

		s = device_detect(first_dev->sane.name, first_dev->connection,
					&status);
		if (s == NULL) {
			DBG(1, "cannot open a perfectly valid device (%s),"
				" please report to the authors\n", name);
			return SANE_STATUS_INVAL;
		}

	} else {

		if (strncmp(name, "net:", 4) == 0) {
			s = device_detect(name, SANE_MAGICOLOR_NET, &status);
			if (s == NULL)
				return status;
		} else if (strncmp(name, "libusb:", 7) == 0) {
			s = device_detect(name, SANE_MAGICOLOR_USB, &status);
			if (s == NULL)
				return status;
		} else {

			/* as a last resort, check for a match
			 * in the device list. This should handle platforms without libusb.
			 */
			if (first_dev == NULL)
				probe_devices();

			s = device_detect(name, SANE_MAGICOLOR_NODEV, &status);
			if (s == NULL) {
				DBG(1, "invalid device name: %s\n", name);
				return SANE_STATUS_INVAL;
			}
		}
	}


	/* s is always valid here */

	DBG(1, "handle obtained\n");

	init_options(s);

	*handle = (SANE_Handle) s;

	status = open_scanner(s);
	if (status != SANE_STATUS_GOOD) {
		free(s);
		return status;
	}

	return status;
}

void
sane_close(SANE_Handle handle)
{
	Magicolor_Scanner *s;

	/*
	 * XXX Test if there is still data pending from
	 * the scanner. If so, then do a cancel
	 */

	s = (Magicolor_Scanner *) handle;

	if (s->fd != -1)
		close_scanner(s);

	free(s);
}

const SANE_Option_Descriptor *
sane_get_option_descriptor(SANE_Handle handle, SANE_Int option)
{
	Magicolor_Scanner *s = (Magicolor_Scanner *) handle;

	if (option < 0 || option >= NUM_OPTIONS)
		return NULL;

	return s->opt + option;
}

static const SANE_String_Const *
search_string_list(const SANE_String_Const *list, SANE_String value)
{
	while (*list != NULL && strcmp(value, *list) != 0)
		list++;

	return ((*list == NULL) ? NULL : list);
}

/*
    Activate, deactivate an option. Subroutines so we can add
    debugging info if we want. The change flag is set to TRUE
    if we changed an option. If we did not change an option,
    then the value of the changed flag is not modified.
*/

static void
activateOption(Magicolor_Scanner *s, SANE_Int option, SANE_Bool *change)
{
	if (!SANE_OPTION_IS_ACTIVE(s->opt[option].cap)) {
		s->opt[option].cap &= ~SANE_CAP_INACTIVE;
		*change = SANE_TRUE;
	}
}

static void
deactivateOption(Magicolor_Scanner *s, SANE_Int option, SANE_Bool *change)
{
	if (SANE_OPTION_IS_ACTIVE(s->opt[option].cap)) {
		s->opt[option].cap |= SANE_CAP_INACTIVE;
		*change = SANE_TRUE;
	}
}

static SANE_Status
getvalue(SANE_Handle handle, SANE_Int option, void *value)
{
	Magicolor_Scanner *s = (Magicolor_Scanner *) handle;
	SANE_Option_Descriptor *sopt = &(s->opt[option]);
	Option_Value *sval = &(s->val[option]);

	DBG(17, "%s: option = %d\n", __func__, option);

	switch (option) {

	case OPT_NUM_OPTS:
	case OPT_BIT_DEPTH:
	case OPT_BRIGHTNESS:
	case OPT_RESOLUTION:
	case OPT_PREVIEW:
	case OPT_TL_X:
	case OPT_TL_Y:
	case OPT_BR_X:
	case OPT_BR_Y:
		*((SANE_Word *) value) = sval->w;
		break;

	case OPT_MODE:
	case OPT_SOURCE:
	case OPT_ADF_MODE:
		strcpy((char *) value, sopt->constraint.string_list[sval->w]);
		break;

	default:
		return SANE_STATUS_INVAL;
	}

	return SANE_STATUS_GOOD;
}


/*
 * Handles setting the source (flatbed, or auto document feeder (ADF)).
 *
 */

static void
change_source(Magicolor_Scanner *s, SANE_Int optindex, char *value)
{
	int force_max = SANE_FALSE;
	SANE_Bool dummy;

	DBG(1, "%s: optindex = %d, source = '%s'\n", __func__, optindex,
	    value);

	if (s->val[OPT_SOURCE].w == optindex)
		return;

	s->val[OPT_SOURCE].w = optindex;

	if (s->val[OPT_TL_X].w == s->hw->x_range->min
	    && s->val[OPT_TL_Y].w == s->hw->y_range->min
	    && s->val[OPT_BR_X].w == s->hw->x_range->max
	    && s->val[OPT_BR_Y].w == s->hw->y_range->max) {
		force_max = SANE_TRUE;
	}

	if (strcmp(ADF_STR, value) == 0) {
		s->hw->x_range = &s->hw->cap->adf_x_range;
		s->hw->y_range = &s->hw->cap->adf_y_range;
		if (s->hw->cap->adf_duplex) {
			activateOption(s, OPT_ADF_MODE, &dummy);
		} else {
			deactivateOption(s, OPT_ADF_MODE, &dummy);
			s->val[OPT_ADF_MODE].w = 0;
		}

		DBG(1, "adf activated (%d)\n",s->hw->cap->adf_duplex);

	} else {
		/* ADF not active */
		s->hw->x_range = &s->hw->cap->fbf_x_range;
		s->hw->y_range = &s->hw->cap->fbf_y_range;

		deactivateOption(s, OPT_ADF_MODE, &dummy);
	}

	s->opt[OPT_BR_X].constraint.range = s->hw->x_range;
	s->opt[OPT_BR_Y].constraint.range = s->hw->y_range;

	if (s->val[OPT_TL_X].w < s->hw->x_range->min || force_max)
		s->val[OPT_TL_X].w = s->hw->x_range->min;

	if (s->val[OPT_TL_Y].w < s->hw->y_range->min || force_max)
		s->val[OPT_TL_Y].w = s->hw->y_range->min;

	if (s->val[OPT_BR_X].w > s->hw->x_range->max || force_max)
		s->val[OPT_BR_X].w = s->hw->x_range->max;

	if (s->val[OPT_BR_Y].w > s->hw->y_range->max || force_max)
		s->val[OPT_BR_Y].w = s->hw->y_range->max;

}

static SANE_Status
setvalue(SANE_Handle handle, SANE_Int option, void *value, SANE_Int *info)
{
	Magicolor_Scanner *s = (Magicolor_Scanner *) handle;
	SANE_Option_Descriptor *sopt = &(s->opt[option]);
	Option_Value *sval = &(s->val[option]);

	SANE_Status status;
	const SANE_String_Const *optval = NULL;
	int optindex = 0;
	SANE_Bool reload = SANE_FALSE;

	DBG(17, "%s: option = %d, value = %p, as word: %d\n", __func__, option, value, *(SANE_Word *) value);

	status = sanei_constrain_value(sopt, value, info);
	if (status != SANE_STATUS_GOOD)
		return status;

	if (info && value && (*info & SANE_INFO_INEXACT)
	    && sopt->type == SANE_TYPE_INT)
		DBG(17, "%s: constrained val = %d\n", __func__,
		    *(SANE_Word *) value);

	if (sopt->constraint_type == SANE_CONSTRAINT_STRING_LIST) {
		optval = search_string_list(sopt->constraint.string_list,
					    (char *) value);
		if (optval == NULL)
			return SANE_STATUS_INVAL;
		optindex = optval - sopt->constraint.string_list;
	}

	switch (option) {

	case OPT_MODE:
	{
		sval->w = optindex;
		/* if binary, then disable the bit depth selection */
		if (optindex == 0) {
			s->opt[OPT_BIT_DEPTH].cap |= SANE_CAP_INACTIVE;
		} else {
			if (s->hw->cap->depth_list[0] == 1)
				s->opt[OPT_BIT_DEPTH].cap |=
					SANE_CAP_INACTIVE;
			else {
				s->opt[OPT_BIT_DEPTH].cap &=
					~SANE_CAP_INACTIVE;
				s->val[OPT_BIT_DEPTH].w =
					mode_params[optindex].depth;
			}
		}
		reload = SANE_TRUE;
		break;
	}

	case OPT_BIT_DEPTH:
		sval->w = *((SANE_Word *) value);
		mode_params[s->val[OPT_MODE].w].depth = sval->w;
		reload = SANE_TRUE;
		break;

	case OPT_RESOLUTION:
		sval->w = *((SANE_Word *) value);
		DBG(17, "setting resolution to %d\n", sval->w);
		reload = SANE_TRUE;
		break;

	case OPT_BR_X:
	case OPT_BR_Y:
		sval->w = *((SANE_Word *) value);
		if (SANE_UNFIX(sval->w) == 0) {
			DBG(17, "invalid br-x or br-y\n");
			return SANE_STATUS_INVAL;
		}
		/* passthru */
	case OPT_TL_X:
	case OPT_TL_Y:
		sval->w = *((SANE_Word *) value);
		DBG(17, "setting size to %f\n", SANE_UNFIX(sval->w));
		if (NULL != info)
			*info |= SANE_INFO_RELOAD_PARAMS;
		break;

	case OPT_SOURCE:
		change_source(s, optindex, (char *) value);
		reload = SANE_TRUE;
		break;

	case OPT_ADF_MODE:
		sval->w = optindex;	/* Simple lists */
		break;

	case OPT_BRIGHTNESS:
	case OPT_PREVIEW:	/* needed? */
		sval->w = *((SANE_Word *) value);
		break;

	default:
		return SANE_STATUS_INVAL;
	}

	if (reload && info != NULL)
		*info |= SANE_INFO_RELOAD_OPTIONS | SANE_INFO_RELOAD_PARAMS;

	DBG(17, "%s: end\n", __func__);

	return SANE_STATUS_GOOD;
}

SANE_Status
sane_control_option(SANE_Handle handle, SANE_Int option, SANE_Action action,
		    void *value, SANE_Int *info)
{
	DBG(17, "%s: action = %x, option = %d\n", __func__, action, option);

	if (option < 0 || option >= NUM_OPTIONS)
		return SANE_STATUS_INVAL;

	if (info != NULL)
		*info = 0;

	switch (action) {
	case SANE_ACTION_GET_VALUE:
		return getvalue(handle, option, value);

	case SANE_ACTION_SET_VALUE:
		return setvalue(handle, option, value, info);

	default:
		return SANE_STATUS_INVAL;
	}

	return SANE_STATUS_INVAL;
}

SANE_Status
sane_get_parameters(SANE_Handle handle, SANE_Parameters *params)
{
	Magicolor_Scanner *s = (Magicolor_Scanner *) handle;

	DBG(5, "%s\n", __func__);

	if (params == NULL)
		DBG(1, "%s: params is NULL\n", __func__);

	/*
	 * If sane_start was already called, then just retrieve the parameters
	 * from the scanner data structure
	 */

	if (!s->eof && s->ptr != NULL) {
		DBG(5, "scan in progress, returning saved params structure\n");
	} else {
		/* otherwise initialize the params structure and gather the data */
		mc_init_parameters(s);
	}

	if (params != NULL)
		*params = s->params;

	print_params(s->params);

	return SANE_STATUS_GOOD;
}

/*
 * This function is part of the SANE API and gets called from the front end to
 * start the scan process.
 */

SANE_Status
sane_start(SANE_Handle handle)
{
	Magicolor_Scanner *s = (Magicolor_Scanner *) handle;
	SANE_Status status;

	DBG(5, "%s\n", __func__);

	/* calc scanning parameters */
	status = mc_init_parameters(s);
	if (status != SANE_STATUS_GOOD)
		return status;

	print_params(s->params);

	/* set scanning parameters; also query the current image
	 * parameters from the sanner and save
	 * them to s->params */
	status = mc_set_scanning_parameters(s);

	if (status != SANE_STATUS_GOOD)
		return status;

	/* if we scan from ADF, check if it is loaded */
	if (strcmp(source_list[s->val[OPT_SOURCE].w], ADF_STR) == 0) {
		status = mc_check_adf(s);
		if (status != SANE_STATUS_GOOD)
			return status;
	}

	/* set the retry count to 0 */
	s->retry_count = 0;

	/* prepare buffer here so that a memory allocation failure
	 * will leave the scanner in a sane state.
	 */
	s->buf = realloc(s->buf, s->block_len);
	if (s->buf == NULL)
		return SANE_STATUS_NO_MEM;

	s->eof = SANE_FALSE;
	s->ptr = s->end = s->buf;
	s->canceling = SANE_FALSE;

	/* start scanning */
	DBG(1, "%s: scanning...\n", __func__);

	status = mc_start_scan(s);

	if (status != SANE_STATUS_GOOD) {
		DBG(1, "%s: start failed: %s\n", __func__,
		    sane_strstatus(status));

		return status;
	}

	return status;
}

/* this moves data from our buffers to SANE */

SANE_Status
sane_read(SANE_Handle handle, SANE_Byte *data, SANE_Int max_length,
	  SANE_Int *length)
{
	SANE_Status status;
	Magicolor_Scanner *s = (Magicolor_Scanner *) handle;

	if (s->buf == NULL || s->canceling)
		return SANE_STATUS_CANCELLED;

	*length = 0;

	status = mc_read(s);

	if (status == SANE_STATUS_CANCELLED) {
		mc_scan_finish(s);
		return status;
	}

	DBG(18, "moving data %p %p, %d (%d lines)\n",
		s->ptr, s->end,
		max_length, max_length / s->params.bytes_per_line);

	mc_copy_image_data(s, data, max_length, length);

	DBG(18, "%d lines read, status: %d\n",
		*length / s->params.bytes_per_line, status);

	/* continue reading if appropriate */
	if (status == SANE_STATUS_GOOD)
		return status;

	mc_scan_finish(s);

	return status;
}

/*
 * void sane_cancel(SANE_Handle handle)
 *
 * Set the cancel flag to true. The next time the backend requests data
 * from the scanner the CAN message will be sent.
 */

void
sane_cancel(SANE_Handle handle)
{
	Magicolor_Scanner *s = (Magicolor_Scanner *) handle;

	s->canceling = SANE_TRUE;
}

/*
 * SANE_Status sane_set_io_mode()
 *
 * not supported - for asynchronous I/O
 */

SANE_Status
sane_set_io_mode(SANE_Handle __sane_unused__ handle,
	SANE_Bool __sane_unused__ non_blocking)
{
	return SANE_STATUS_UNSUPPORTED;
}

/*
 * SANE_Status sane_get_select_fd()
 *
 * not supported - for asynchronous I/O
 */

SANE_Status
sane_get_select_fd(SANE_Handle __sane_unused__ handle,
	SANE_Int __sane_unused__ *fd)
{
	return SANE_STATUS_UNSUPPORTED;
}
