/*
 * Prototypes for Magicolor commands
 *
 * (C) 2010 Reinhold Kainhofer <reinhold@kainhofer.com>
 *
 * Based on the epson2 sane backend:
 * Based on Kazuhiro Sasayama previous
 * Work on epson.[ch] file from the SANE package.
 * Please see those files for original copyrights.
 * Copyright (C) 2006 Tower Technologies
 * Author: Alessandro Zummo <a.zummo@towertech.it>
 *
 * This file is part of the SANE package.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, version 2.
 */

SANE_Status cmd_request_error(SANE_Handle handle);  /* 0x03 09 command */
SANE_Status cmd_request_status(SANE_Handle handle, unsigned char *buf); /* 0x03 0d command */
SANE_Status cmd_start_scan (SANE_Handle handle, size_t value); /* 0x03 08 command */
SANE_Status cmd_cancel_scan (SANE_Handle handle); /* 0x03 0a command */
SANE_Status cmd_get_scanning_parameters(SANE_Handle handle,
					SANE_Frame *format, SANE_Int *depth,
					SANE_Int *data_pixels, SANE_Int *pixels_per_line,
					SANE_Int *lines); /* 0x03 0b command, input buffer seems to be 0x00 always */
SANE_Status cmd_set_scanning_parameters(SANE_Handle handle,
					unsigned char resolution,
					unsigned char color_mode,
					unsigned char brightness,
					unsigned char contrast,
					int tl_x, int tl_y, int width, int height,
					unsigned char source); /* 0x03 0c command */
SANE_Status cmd_read_data (SANE_Handle handle, unsigned char *buf, size_t len); /* 0x03 0e command */
SANE_Status cmd_request_push_button_status(SANE_Handle handle,
					   unsigned char *bstatus);
SANE_Status cmd_finish_scan (SANE_Handle handle); /* 0x03 12 command, TODO: really FINISH? */

/* TODO: 0x03 0f command (unknown), 0x03 10 command (set button wait) */