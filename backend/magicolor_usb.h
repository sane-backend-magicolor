#ifndef _MAGICOLOR_USB_H_
#define _MAGICOLOR_USB_H_

#define SANE_MAGICOLOR_VENDOR_ID	(0x132b)

extern SANE_Word sanei_magicolor_usb_product_ids[];

extern int sanei_magicolor_getNumberOfUSBProductIds (void);

#endif
