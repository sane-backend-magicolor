#include <sys/types.h>
#include "../include/sane/sanei_usb.h"
#include "magicolor_usb.h"


SANE_Word sanei_magicolor_usb_product_ids[] = {
  0x2089, /* magicolor 1690MF */
  0				/* last entry - this is used for devices that are specified
				   in the config file as "usb <vendor> <product>" */
};

int
sanei_magicolor_getNumberOfUSBProductIds (void)
{
  return sizeof (sanei_magicolor_usb_product_ids) / sizeof (SANE_Word);
}
