/*
 * magicolor_net.c - SANE library for Magicolor scanners.
 *
 * (C) 2010 Reinhold Kainhofer <reinhold@kainhofer.com>
 *
 * Based on the epson2 sane backend:
 * Copyright (C) 2006 Tower Technologies
 * Author: Alessandro Zummo <a.zummo@towertech.it>
 * This file is part of the SANE package.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, version 2.
 */

#define DEBUG_DECLARE_ONLY

#include "sane/config.h"

#include "sane/sane.h"
#include "sane/saneopts.h"
#include "sane/sanei_tcp.h"
#include "sane/sanei_config.h"
#include "sane/sanei_backend.h"

#include "magicolor.h"
#include "magicolor_net.h"

#include "byteorder.h"

#include "sane/sanei_debug.h"
#include <poll.h>

/* We don't have a packet wrapper, which holds packet size etc., so we
   don't have to use a *read_raw and a *_read function... */
int
sanei_magicolor_net_read(struct Magicolor_Scanner *s, unsigned char *buf, size_t wanted,
		       SANE_Status * status)
{
	size_t size, read = 0;
	struct pollfd fds[1];

	*status = SANE_STATUS_GOOD;

	/* First poll for data-to-be-read (using a 20 seconds timeout) */
	fds[0].fd = s->fd;
	fds[0].events = POLLIN;
	if (poll (fds, 1, 20000) <= 0) {
		/* No data available for reading after 5 seconds timeout */
		*status = SANE_STATUS_IO_ERROR;
		return read;
	}

	while (read < wanted) {
		size = sanei_tcp_read(s->fd, buf + read, wanted - read);

		if (size == 0)
			break;

		read += size;
	}

	if (read < wanted)
		*status = SANE_STATUS_IO_ERROR;

	return read;
}

/* We need to optionally pad the buffer with 0x00 to send 64-byte chunks.
   On the other hand, the 0x04 commands don't need this, so we need two
   functions, one *_write function that pads the buffer and then calls
    *_write_raw */
int
sanei_magicolor_net_write_raw(struct Magicolor_Scanner *s,
const unsigned char *buf, size_t buf_size,
			SANE_Status *status)
{
	sanei_tcp_write(s->fd, buf, buf_size);
	/* TODO: Check whether sending failed... */

	*status = SANE_STATUS_GOOD;
	return buf_size;
}

int
sanei_magicolor_net_write(struct Magicolor_Scanner *s,
const unsigned char *buf, size_t buf_size,
			SANE_Status *status)
{
	size_t len = 64;
	unsigned char *new_buf = malloc(len);
	if (!new_buf) {
		*status = SANE_STATUS_NO_MEM;
		return 0;
	}
	memset(new_buf, 0x00, len);
	if (buf_size > len)
		buf_size = len;
	if (buf_size)
		memcpy(new_buf, buf, buf_size);
	return sanei_magicolor_net_write_raw (s, new_buf, len, status);
}

SANE_Status
sanei_magicolor_net_open(struct Magicolor_Scanner *s)
{
	SANE_Status status;
	unsigned char buf[5];

	ssize_t read;
	struct timeval tv;
	struct MagicolorCmd *cmd = s->hw->cmd;

	tv.tv_sec = 5;
	tv.tv_usec = 0;

	setsockopt(s->fd, SOL_SOCKET, SO_RCVTIMEO, (char *)&tv,  sizeof(tv));

	DBG(1, "%s\n", __func__);

	/* the scanner sends a kind of welcome msg */
	read = sanei_magicolor_net_read(s, buf, 3, &status);
	if (read != 3)
		return SANE_STATUS_IO_ERROR;
	if (buf[0] != cmd->net_wrapper_cmd || buf[1] != cmd->net_welcome) {
		DBG (32, "Invalid welcome message received, Expected 0x%02x %02x 00, but got 0x%02x %02x %02x\n",
			cmd->net_wrapper_cmd, cmd->net_welcome, buf[0], buf[1], buf[2]);
		return SANE_STATUS_IO_ERROR;
	} else if (buf[2] != 0x00) {
		/* TODO: Handle response "04 00 01", indicating an error! */
		DBG (32, "Welcome message received, busy status %02x\n", buf[2]);
		/* TODO: Return a human-readable error message (Unable to connect to scanner, scanner is not ready) */
		return SANE_STATUS_DEVICE_BUSY;
	}

	buf[0] = cmd->net_wrapper_cmd;
	buf[1] = cmd->net_lock;
	buf[2] = 0x00;
	/* Copy the device's USB id to bytes 3-4: */
	htole16a(&buf[3], s->hw->cap->id);
	DBG(32, "Proper welcome message received, locking the scanner...\n");
	sanei_magicolor_net_write_raw(s, buf, 5, &status);

	read = sanei_magicolor_net_read(s, buf, 3, &status);
	if (read != 3)
		return SANE_STATUS_IO_ERROR;
	if (buf[0] != cmd->net_wrapper_cmd || buf[1] != cmd->net_lock_ack || buf[2] != 0x00) {
		DBG (32, "Welcome message received, Expected 0x%x %x 00, but got 0x%x %x %x\n",
			cmd->net_wrapper_cmd, cmd->net_lock_ack, buf[0], buf[1], buf[2]);
		return SANE_STATUS_IO_ERROR;
	}

	DBG(32, "scanner locked\n");

	return status;
}

SANE_Status
sanei_magicolor_net_close(struct Magicolor_Scanner *s)
{
	SANE_Status status;
	struct MagicolorCmd *cmd = s->hw->cmd;
	unsigned char buf[3];

	DBG(1, "%s\n", __func__);
	buf[0] = cmd->net_wrapper_cmd;
	buf[1] = cmd->net_unlock;
	buf[2] = 0x00;
	sanei_magicolor_net_write_raw(s, buf, 3, &status);
	return status;
}
