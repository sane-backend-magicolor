/*
 * magicolor.c - SANE library for Magicolor scanners.
 *
 * (C) 2010 Reinhold Kainhofer <reinhold@kainhofer.com>
 *
 * Based on the epson2 sane backend:
 * Based on Kazuhiro Sasayama previous
 * Work on magicolor.[ch] file from the SANE package.
 * Please see those files for additional copyrights.
 * Copyright (C) 2006-09 Tower Technologies
 * Author: Alessandro Zummo <a.zummo@towertech.it>
 *
 * This file is part of the SANE package.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, version 2.
 */

#define DEBUG_DECLARE_ONLY

#include "sane/config.h"

#include <unistd.h>
#include <sys/select.h>
#include <math.h>


#include "byteorder.h"

#include "magicolor.h"
#include "magicolor-ops.h"

#include "magicolor-io.h"
#include "magicolor-commands.h"

/*          Scanner command type
 *          |     Start scan
 *          |     |     Poll for error
 *          |     |     |     Stop scan?
 *          |     |     |     |     Query image parameters
 *          |     |     |     |     |     set scan parameters
 *          |     |     |     |     |     |     Get status?
 *          |     |     |     |     |     |     |     Read scanned data
 *          |     |     |     |     |     |     |     |     Unknown
 *          |     |     |     |     |     |     |     |     |     Unknown
 *          |     |     |     |     |     |     |     |     |     |     Net wrapper command type
 *          |     |     |     |     |     |     |     |     |     |     |     Net Welcome
 *          |     |     |     |     |     |     |     |     |     |     |     |     Net Lock
 *          |     |     |     |     |     |     |     |     |     |     |     |     |     Net Lock ACK
 *          |     |     |     |     |     |     |     |     |     |     |     |     |     |     Net Unlock
 *          |     |     |     |     |     |     |     |     |     |     |     |     |     |     |
*/
static struct MagicolorCmd magicolor_cmd[] = {
  {"mc1690mf", 0x03, 0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f, 0x12, 0x04, 0x00, 0x01, 0x02, 0x03}
};

static SANE_Int magicolor_default_resolutions[] = {150, 300, 600};
static SANE_Int magicolor_default_depths[] = {1,8};

static struct MagicolorCap magicolor_cap[] = {

  /* KONICA MINOLTA magicolor 1690MF, USB ID 0x123b:2089 */
  {
      0x2089, "mc1690mf", "KONICA MINOLTA magicolor 1690MF", "1.3.6.1.4.1.183341.1.1.2.1.32.3.2",
      600, {150, 600, 0}, magicolor_default_resolutions, 3,  /* 600 dpi max, 3 resolutions */
      8, magicolor_default_depths,                          /* color depth 8 default, 1 and 8 possible */
      {1, 9, 0},                                             /* brightness ranges (TODO!) */
      {0, SANE_FIX(0x13f8 * MM_PER_INCH / 600), 0}, {0, SANE_FIX(0x1b9c * MM_PER_INCH / 600), 0},                             /* FBF x/y ranges (TODO!) */
      SANE_TRUE, SANE_FALSE, /* non-duplex ADF, x/y ranges (TODO!) */
      {0, SANE_FIX(0x1390 * MM_PER_INCH / 600), 0}, {0, SANE_FIX(0x20dc * MM_PER_INCH / 600), 0},
  }

};

struct MagicolorCap *
mc_get_device_from_identification (const char*ident)
{
	int n;
	for (n = 0; n < NELEMS (magicolor_cap); n++) {
		if (strcmp (magicolor_cap[n].model, ident) || strcmp (magicolor_cap[n].OID, ident))
			return &magicolor_cap[n];
	}
	return NULL;
}

/* param list holding the color mode and depth binary values for the 0x03 0c command */
extern struct mode_param mode_params[];


/* Define the different scan sources */
#define FBF_STR	SANE_I18N("Flatbed")
#define ADF_STR	SANE_I18N("Automatic Document Feeder")

/*
 * source list need one dummy entry (save device settings is crashing).
 * NOTE: no const - this list gets created while exploring the capabilities
 * of the scanner.
 */
extern SANE_String_Const source_list[];


void
mc_dev_init(Magicolor_Device *dev, const char *devname, int conntype)
{
	DBG(5, "%s\n", __func__);

	dev->name = NULL;
	dev->model = NULL;
	dev->connection = conntype;

	dev->model_id = 0;

	dev->sane.name = devname;
	dev->sane.model = NULL;

	dev->sane.type = "flatbed scanner";
	dev->sane.vendor = "Magicolor";

	dev->cap = &magicolor_cap[MAGICOLOR_CAP_DEFAULT];
	dev->cmd = &magicolor_cmd[MAGICOLOR_LEVEL_DEFAULT];

	/* Change default level when using a network connection */
	if (dev->connection == SANE_MAGICOLOR_NET)
		dev->cmd = &magicolor_cmd[MAGICOLOR_LEVEL_NET];

	dev->last_res = 0;
	dev->last_res_preview = 0;	/* set resolution to safe values */

}

SANE_Status
mc_dev_post_init(struct Magicolor_Device *dev)
{
	DBG(5, "%s\n", __func__);
	NOT_USED (dev);

	/* Correct device parameters if needed */
	/* nothing needed yet! */

	return SANE_STATUS_GOOD;
}

void
mc_set_device (SANE_Handle handle, unsigned int device)
{
	Magicolor_Scanner *s = (Magicolor_Scanner *) handle;
	Magicolor_Device *dev = s->hw;
	const char* cmd_level;
	int n;

	DBG(1, "%s: 0x%x\n", __func__, device);

	for (n = 0; n < NELEMS (magicolor_cap); n++) {
		if (magicolor_cap[n].id == device)
			break;
	}
	if (n < NELEMS(magicolor_cap)) {
		dev->cap = &magicolor_cap[n];
	} else {
		dev->cap = &magicolor_cap[MAGICOLOR_CAP_DEFAULT];
		DBG(1, " unknown device 0x%x, using default %s\n",
		    device, dev->cap->model);
	}
	mc_set_model (s, dev->cap->model, strlen (dev->cap->model));

	cmd_level = dev->cap->cmds;
	/* set command type and level */
	for (n = 0; n < NELEMS(magicolor_cmd); n++) {
		if (!strcmp(cmd_level, magicolor_cmd[n].level))
			break;
	}

	if (n < NELEMS(magicolor_cmd)) {
		dev->cmd = &magicolor_cmd[n];
	} else {
		dev->cmd = &magicolor_cmd[MAGICOLOR_LEVEL_DEFAULT];
		DBG(1, " unknown command level %s, using %s\n",
		    cmd_level, dev->cmd->level);
	}
}

SANE_Bool
mc_is_model(Magicolor_Device *dev, const char *model)
{
	if (dev->model == NULL)
		return SANE_FALSE;

	if (strncmp(dev->model, model, strlen(model)) == 0)
		return SANE_TRUE;

	return SANE_FALSE;
}

SANE_Status
mc_set_model(Magicolor_Scanner * s, const char *model, size_t len)
{
	unsigned char *buf;
	unsigned char *p;
	struct Magicolor_Device *dev = s->hw;

	buf = malloc(len + 1);
	if (buf == NULL)
		return SANE_STATUS_NO_MEM;

	memcpy(buf, model, len);
	buf[len] = '\0';

	p = &buf[len - 1];

	while (*p == ' ') {
		*p = '\0';
		p--;
	}

	if (dev->model)
		free(dev->model);

	dev->model = strndup((const char *) buf, len);
	dev->sane.model = dev->model;

	DBG(10, "%s: model is '%s'\n", __func__, dev->model);

	free(buf);

	return SANE_STATUS_GOOD;
}

SANE_Status
mc_add_resolution(Magicolor_Device *dev, int r)
{
	dev->cap->res_list_size++;
	dev->cap->res_list = (SANE_Int *) realloc(dev->cap->res_list,
						  dev->cap->res_list_size *
						  sizeof(SANE_Word));

	DBG(10, "%s: add (dpi): %d\n", __func__, r);

	if (dev->cap->res_list == NULL)
		return SANE_STATUS_NO_MEM;

	dev->cap->res_list[dev->cap->res_list_size - 1] = (SANE_Int) r;

	return SANE_STATUS_GOOD;
}

void
mc_add_depth(Magicolor_Device * dev, SANE_Word depth)
{
	if (depth > dev->cap->maxDepth)
		dev->cap->maxDepth = depth;

	dev->cap->depth_list[0]++;
	dev->cap->depth_list[dev->cap->depth_list[0]] = depth;
}

SANE_Status
mc_discover_capabilities(Magicolor_Scanner *s)
{
	SANE_Status status;
	Magicolor_Device *dev = s->hw;

	SANE_String_Const *source_list_add = source_list;

	DBG(5, "%s\n", __func__);

	/* always add flatbed */
	*source_list_add++ = FBF_STR;
	/* TODO: How can I check for existence of an ADF??? */
	if (dev->cap->ADF)
		*source_list_add++ = ADF_STR;

	/* TODO: Is there any capability that we can extract from the
	 *       device by some scanne command? So far, it looks like
	 *       the device does not support any reporting. I don't even
	 *       see a way to determine which device we are talking to!
	 */


	/* request error status */
	status = cmd_request_error(s);
	if (status != SANE_STATUS_GOOD)
		return status;

	dev->x_range = &dev->cap->fbf_x_range;
	dev->y_range = &dev->cap->fbf_y_range;

	DBG(5, "   x-range: %f %f\n", SANE_UNFIX(dev->x_range->min), SANE_UNFIX(dev->x_range->max));
	DBG(5, "   y-range: %f %f\n", SANE_UNFIX(dev->y_range->min), SANE_UNFIX(dev->y_range->max));


	DBG(5, "End of %s, status:%s\n", __func__, sane_strstatus(status));
	*source_list_add = NULL; /* add end marker to source list */
	return status;
}

/* Call the 0x03 0c command to set scanning parameters from the s->opt list */
SANE_Status
mc_set_scanning_parameters(Magicolor_Scanner * s)
{
	SANE_Status status;
	unsigned char rs, source, brightness;
	struct mode_param *mparam = &mode_params[s->val[OPT_MODE].w];
	SANE_Int scan_pixels_per_line;

	DBG(1, "%s\n", __func__);

	/* Find the resolution in the res list and assign the index to buf[1] */
	for (rs=0; rs < s->hw->cap->res_list_size; rs++ ) {
		if ( s->val[OPT_RESOLUTION].w == s->hw->cap->res_list[rs] )
			break;
	}

	if (SANE_OPTION_IS_ACTIVE(s->opt[OPT_BRIGHTNESS].cap)) {
		brightness = s->val[OPT_BRIGHTNESS].w;
	} else {
		brightness = 5;
	}

	/* ADF used? */
	if (strcmp(source_list[s->val[OPT_SOURCE].w], ADF_STR) == 0) {
		/* Use ADF */
		source = 0x01;
	} else {
		source = 0x00;
	}

	/* TODO: Any way to set PREVIEW??? */

	/* Remaining bytes unused */
	status = cmd_set_scanning_parameters(s,
					     rs, mparam->flags,  /* res, color mode */
					     brightness, 0xff, /* brightness, contrast? */
					     s->left, s->top,  /* top/left start */
					     s->width, s->height, /* extent */
					     source); /* source */

	if (status != SANE_STATUS_GOOD)
		DBG (2, "%s: Command cmd_set_scanning_parameters failed, %s\n",
		     __func__, sane_strstatus(status));

	/* Now query the scanner for the current image parameters */
	status = cmd_get_scanning_parameters (s,
			&s->params.format, &s->params.depth,
			&scan_pixels_per_line,
			&s->params.pixels_per_line, &s->params.lines);
	if (status != SANE_STATUS_GOOD)
		DBG (2, "%s: Command cmd_get_scanning_parameters failed, %s\n",
		     __func__, sane_strstatus(status));

	/* Calculate how many bytes are really used per line */
	s->params.bytes_per_line = ceil (s->params.pixels_per_line * s->params.depth / 8.0);
	if (s->val[OPT_MODE].w == MODE_COLOR)
		s->params.bytes_per_line *= 3;

	/* Calculate how many bytes per line will be returned by the scanner.
	 * The values needed for this are returned by get_scannign_parameters */
	s->scan_bytes_per_line = ceil (scan_pixels_per_line * s->params.depth / 8.0);
	if (s->val[OPT_MODE].w == MODE_COLOR) {
		s->scan_bytes_per_line *= 3;
	}
	s->data_len = s->params.lines * s->scan_bytes_per_line;

	status = mc_setup_block_mode (s);
	if (status != SANE_STATUS_GOOD)
		DBG (2, "%s: Command mc_setup_block_mode failed, %s\n",
		     __func__, sane_strstatus(status));

	DBG (1, "%s: bytes_read  in line: %d\n", __func__, s->bytes_read_in_line);

	return status;
}

SANE_Status
mc_setup_block_mode (Magicolor_Scanner *s)
{
	/* block_len should always be a multiple of bytes_per_line, so
	 * we retrieve only whole lines at once */
	s->block_len = (int)(0xff00/s->scan_bytes_per_line) * s->scan_bytes_per_line;
	s->blocks = s->data_len / s->block_len;
	s->last_len = s->data_len - (s->blocks * s->block_len);
	if (s->last_len>0)
		s->blocks++;
	DBG(1, "%s: block_len=0x%x, last_len=0x%0x, blocks=%d\n", __func__, s->block_len, s->last_len, s->blocks);
	s->counter = 0;
	s->bytes_read_in_line = 0;
	if (s->line_buffer)
		free(s->line_buffer);
	s->line_buffer = malloc(s->scan_bytes_per_line);
	if (s->line_buffer == NULL) {
		DBG(1, "out of memory (line %d)\n", __LINE__);
		return SANE_STATUS_NO_MEM;
	}


	DBG (2, " %s: Setup block mode - scan_bytes_per_line=%d, pixels_per_line=%d, depth=%d, data_len=%x, block_len=%x, blocks=%d, last_len=%x\n",
		__func__, s->scan_bytes_per_line, s->params.pixels_per_line, s->params.depth, s->data_len, s->block_len, s->blocks, s->last_len);
	DBG (1, "%s: bytes_read  in line: %d\n", __func__, s->bytes_read_in_line);
	return SANE_STATUS_GOOD;
}

SANE_Status
mc_check_adf(Magicolor_Scanner * s)
{
	SANE_Status status;
	unsigned char buf[0x0b];

	DBG(5, "%s\n", __func__);

	status = cmd_request_status(s, buf);
	if (status != SANE_STATUS_GOOD)
		return status;

	if (!(buf[1] & ADF_LOADED))
		return SANE_STATUS_NO_DOCS;

	/* TODO: Check for jam in ADF */
	return SANE_STATUS_GOOD;
}

SANE_Status
mc_scan_finish(Magicolor_Scanner * s)
{
	SANE_Status status;
	DBG(5, "%s\n", __func__);

	if (s->line_buffer)
		free (s->line_buffer);
	s->line_buffer = NULL;
	free(s->buf);
	s->buf = s->end = s->ptr = NULL;

	/* TODO: Any magicolor command for "scan finished"? */
	status = cmd_finish_scan (s);

	/* Send a cancel to the scanner, just to make sure it really stops
	 * waiting for read commands. This happens if either some read
	 * fails while the scanner still has data, or if we have messed
	 * up the calculation of the data size */
/*	status = cmd_cancel_scan (s);
	if (status != SANE_STATUS_GOOD) {
		return status;
	}
*/
	status = cmd_request_error(s);
	if (status != SANE_STATUS_GOOD)
		cmd_cancel_scan (s);
		return status;

	/* XXX required? */
	/* TODO:	cmd_reset(s);*/
	return SANE_STATUS_GOOD;
}

void
mc_copy_image_data(Magicolor_Scanner * s, SANE_Byte * data, SANE_Int max_length,
		   SANE_Int * length)
{
	DBG (1, "%s: bytes_read  in line: %d\n", __func__, s->bytes_read_in_line);
	if (s->params.format == SANE_FRAME_RGB) {
		SANE_Int bytes_available, scan_pixels_per_line = s->scan_bytes_per_line/3;
		*length = 0;

		while ((max_length >= s->params.bytes_per_line) && (s->ptr < s->end)) {
			SANE_Int bytes_to_copy = s->scan_bytes_per_line - s->bytes_read_in_line;
			/* First, fill the line buffer for the current line: */
			bytes_available = (s->end - s->ptr);
			/* Don't copy more than we have buffer and available */
			if (bytes_to_copy > bytes_available)
				bytes_to_copy = bytes_available;

			if (bytes_to_copy > 0) {
				memcpy (s->line_buffer + s->bytes_read_in_line, s->ptr, bytes_to_copy);
				s->ptr += bytes_to_copy;
				s->bytes_read_in_line += bytes_to_copy;
			}

			/* We have filled as much as possible of the current line
			 * with data from the scanner. If we have a complete line,
			 * copy it over. */
			if ((s->bytes_read_in_line >= s->scan_bytes_per_line) &&
			    (s->params.bytes_per_line <= max_length))
			{
				SANE_Int i;
				SANE_Byte *line = s->line_buffer;
				*length += s->params.bytes_per_line;
				for (i=0; i< s->params.pixels_per_line; ++i) {
					*data++ = line[0];
					*data++ = line[scan_pixels_per_line];
					*data++ = line[2 * scan_pixels_per_line];
					line++;
				}
				max_length -= s->params.bytes_per_line;
				s->bytes_read_in_line -= s->scan_bytes_per_line;
			}
		}

	} else {
		/* B/W and Grayscale use the same structure, so we use the same code */
		SANE_Int bytes_available;
		*length = 0;

		while ((max_length != 0) && (s->ptr < s->end)) {
			SANE_Int bytes_to_skip, bytes_to_copy;
			bytes_available = (s->end - s->ptr);
			bytes_to_copy = s->params.bytes_per_line - s->bytes_read_in_line;
			bytes_to_skip = s->scan_bytes_per_line - s->bytes_read_in_line;

			/* Don't copy more than we have buffer */
			if (bytes_to_copy > max_length) {
				bytes_to_copy = max_length;
				bytes_to_skip = max_length;
			}

			/* Don't copy/skip more bytes than we have read in */
			if (bytes_to_copy > bytes_available)
				bytes_to_copy = bytes_available;
			if (bytes_to_skip > bytes_available)
				bytes_to_skip = bytes_available;

			if (bytes_to_copy > 0) {
				/* we have not yet copied all pixels of the line */
				memcpy (data, s->ptr, bytes_to_copy);
				max_length -= bytes_to_copy;
				*length += bytes_to_copy;
				data += bytes_to_copy;
			}
			if (bytes_to_skip > 0) {
				s->ptr += bytes_to_skip;
				s->bytes_read_in_line += bytes_to_skip;
			}
			if (s->bytes_read_in_line >= s->scan_bytes_per_line)
				s->bytes_read_in_line -= s->scan_bytes_per_line;

		}
	}
}

SANE_Status
mc_init_parameters(Magicolor_Scanner * s)
{
	int dpi, optres;
	struct mode_param *mparam;

	DBG(5, "%s\n", __func__);

	memset(&s->params, 0, sizeof(SANE_Parameters));

	dpi = s->val[OPT_RESOLUTION].w;
	optres = s->hw->cap->optical_res;

	mparam = &mode_params[s->val[OPT_MODE].w];

	if (SANE_UNFIX(s->val[OPT_BR_Y].w) == 0 ||
		SANE_UNFIX(s->val[OPT_BR_X].w) == 0)
		return SANE_STATUS_INVAL;

	/* TODO: Use OPT_RESOLUTION or fixed 600dpi for left/top/width/height? */
	s->left = ((SANE_UNFIX(s->val[OPT_TL_X].w) / MM_PER_INCH) *
		optres) + 0.5;

	s->top = ((SANE_UNFIX(s->val[OPT_TL_Y].w) / MM_PER_INCH) *
		optres) + 0.5;

	s->width =
		((SANE_UNFIX(s->val[OPT_BR_X].w -
			   s->val[OPT_TL_X].w) / MM_PER_INCH) * optres) + 0.5;

	s->height =
		((SANE_UNFIX(s->val[OPT_BR_Y].w -
			   s->val[OPT_TL_Y].w) / MM_PER_INCH) * optres) + 0.5;

	s->params.pixels_per_line = s->width * dpi / optres + 0.5;
	s->params.lines = s->height * dpi / optres + 0.5;


	DBG(1, "%s: resolution = %d, preview = %d\n",
		__func__, dpi, s->val[OPT_PREVIEW].w);

	DBG(1, "%s: %p %p tlx %f tly %f brx %f bry %f [mm]\n",
	    __func__, (void *) s, (void *) s->val,
	    SANE_UNFIX(s->val[OPT_TL_X].w), SANE_UNFIX(s->val[OPT_TL_Y].w),
	    SANE_UNFIX(s->val[OPT_BR_X].w), SANE_UNFIX(s->val[OPT_BR_Y].w));

	/*
	 * The default color depth is stored in mode_params.depth:
	 */
	DBG(1, " %s, vor depth\n", __func__);

	if (mode_params[s->val[OPT_MODE].w].depth == 1)
		s->params.depth = 1;
	else
		s->params.depth = s->val[OPT_BIT_DEPTH].w;

	s->params.last_frame = SANE_TRUE;

	s->params.bytes_per_line = ceil (s->params.depth * s->params.pixels_per_line / 8.0);

	switch (s->val[OPT_MODE].w) {
	case MODE_BINARY:
	case MODE_GRAY:
		s->params.format = SANE_FRAME_GRAY;
		break;
	case MODE_COLOR:
		s->params.format = SANE_FRAME_RGB;
		s->params.bytes_per_line *= 3;
		break;
	}

	/*
	 * If (s->top + s->params.lines) is larger than the max scan area, reset
	 * the number of scan lines:
	 * XXX: precalculate the maximum scanning area elsewhere (use dev max_y)
	 */
	/* TODO! */
#if 0
	if (SANE_UNFIX(s->val[OPT_BR_Y].w) / MM_PER_INCH * dpi <
	    (s->params.lines + s->top)) {
		s->params.lines =
			((int) SANE_UNFIX(s->val[OPT_BR_Y].w) / MM_PER_INCH *
			 dpi + 0.5) - s->top;
	}
#endif

	DBG(1, " %s, vor ende\n", __func__);
	DBG(1, "%s: Parameters are format=%d, bytes_per_line=%d, lines=%d\n", __func__, s->params.format, s->params.bytes_per_line, s->params.lines);
	return (s->params.lines > 0) ? SANE_STATUS_GOOD : SANE_STATUS_INVAL;
}

SANE_Status
mc_start_scan(Magicolor_Scanner * s)
{
	/* TODO: correct value of param */
	SANE_Status status = cmd_start_scan (s, s->data_len);
	if (status != SANE_STATUS_GOOD ) {
		DBG (1, "%s: starting the scan failed (%s)\n", __func__, sane_strstatus(status));
	}
	return status;
}

SANE_Status
mc_read(struct Magicolor_Scanner *s)
{
	SANE_Status status = SANE_STATUS_GOOD;
	ssize_t buf_len = 0;

	/* did we passed everything we read to sane? */
	if (s->ptr == s->end) {

		if (s->eof)
			return SANE_STATUS_EOF;

		s->counter++;
		buf_len = s->block_len;

		if (s->counter == s->blocks && s->last_len)
			buf_len = s->last_len;

		DBG(18, "%s: block %d/%d, size %lu\n", __func__,
			s->counter, s->blocks,
			(unsigned long) buf_len);

		/* receive image data + error code */
		status = cmd_read_data (s, s->buf, buf_len);
		if (status != SANE_STATUS_GOOD) {
			DBG (1, "%s: Receiving image data failed (%s)\n",
					__func__, sane_strstatus(status));
			cmd_cancel_scan(s);
			return status;
		}

		DBG(18, "%s: successfully read %lu bytes\n", __func__, (unsigned long) buf_len);

		if (s->counter < s->blocks) {
			if (s->canceling) {
				cmd_cancel_scan(s);
				return SANE_STATUS_CANCELLED;
			}
		} else
			s->eof = SANE_TRUE;

		s->end = s->buf + buf_len;
		s->ptr = s->buf;
	}

	return status;
}

