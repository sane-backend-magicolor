#ifndef _MAGICOLOR_NET_H_
#define _MAGICOLOR_NET_H_

#include <sys/types.h>
#include "../include/sane/sane.h"

#define MAGICOLOR_SNMP_SYSDESCR_OID  ".1.3.6.1.2.1.1.1.0"
#define MAGICOLOR_SNMP_SYSOBJECT_OID ".1.3.6.1.2.1.1.2.0"
#define MAGICOLOR_SNMP_MAC_OID       ".1.3.6.1.2.1.2.2.1.6.1"
#define MAGICOLOR_SNMP_DEVICE_TREE   ".1.3.6.1.4.1.18334.1.1.1.1.1"

extern int sanei_magicolor_net_read(struct Magicolor_Scanner *s,
				unsigned char *buf, size_t wanted,
				SANE_Status * status);
extern int sanei_magicolor_net_write(struct Magicolor_Scanner *s,
				     const unsigned char *buf, size_t buf_size,
				     SANE_Status *status);
extern int sanei_magicolor_net_write_raw(struct Magicolor_Scanner *s,
				     const unsigned char *buf, size_t buf_size,
				     SANE_Status *status);
extern SANE_Status sanei_magicolor_net_open(struct Magicolor_Scanner *s);
extern SANE_Status sanei_magicolor_net_close(struct Magicolor_Scanner *s);

#endif
