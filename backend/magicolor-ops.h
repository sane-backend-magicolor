/*
 * magicolor.c - SANE library for Magicolor scanners.
 *
 * (C) 2010 Reinhold Kainhofer <reinhold@kainhofer.com>
 *
 * Based on the epson2 sane backend:
 * Based on Kazuhiro Sasayama previous
 * Work on epson.[ch] file from the SANE package.
 * Please see those files for additional copyrights.
 * Copyright (C) 2006-07 Tower Technologies
 * Author: Alessandro Zummo <a.zummo@towertech.it>
 *
 * This file is part of the SANE package.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, version 2.
 */

extern struct MagicolorCap *
mc_get_device_from_identification (const char*ident);

extern void mc_dev_init(Magicolor_Device *dev, const char *devname, int conntype);
extern SANE_Status mc_dev_post_init(struct Magicolor_Device *dev);

extern void mc_set_device(SANE_Handle handle, unsigned int device);
extern SANE_Status mc_set_model(Magicolor_Scanner *s, const char *model, size_t len);
extern SANE_Bool mc_is_model(Magicolor_Device *dev, const char *model);

extern SANE_Status mc_add_resolution(Magicolor_Device *dev, int r);
extern void mc_add_depth(Magicolor_Device *dev, SANE_Word depth);

extern SANE_Status mc_discover_capabilities(Magicolor_Scanner *s);

extern SANE_Status mc_init_parameters(Magicolor_Scanner *s);
extern SANE_Status mc_set_scanning_parameters(Magicolor_Scanner *s);
extern SANE_Status mc_setup_block_mode (Magicolor_Scanner *s);

extern SANE_Status mc_check_adf(Magicolor_Scanner *s);

extern SANE_Status mc_start_scan(Magicolor_Scanner *s);
extern SANE_Status mc_read(struct Magicolor_Scanner *s);
extern void mc_copy_image_data(Magicolor_Scanner *s, SANE_Byte *data, SANE_Int max_length,
		   SANE_Int *length);
extern SANE_Status mc_scan_finish(Magicolor_Scanner *s);
