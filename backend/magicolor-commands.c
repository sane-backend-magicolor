/*
 * Commands for Magicolor scanners
 *
 * (C) 2010 Reinhold Kainhofer <reinhold@kainhofer.com>
 *
 * Based on the epson2 sane backend:
 * Based on Kazuhiro Sasayama previous
 * Work on epson.[ch] file from the SANE package.
 * Please see those files for original copyrights.
 * Copyright (C) 2006 Tower Technologies
 * Author: Alessandro Zummo <a.zummo@towertech.it>
 *
 * This file is part of the SANE package.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, version 2.
 */

#define DEBUG_DECLARE_ONLY

#include "sane/config.h"

#include <byteorder.h>
#include <math.h>

#include "magicolor.h"
#include "magicolor-io.h"
#include "magicolor-commands.h"


/* 0x03 09 01 - Request last error
 * <- Information block (0x00 for OK, 0x01 for ERROR)
 */

SANE_Status
cmd_request_error (SANE_Handle handle)
{
	Magicolor_Scanner *s = (Magicolor_Scanner *) handle;
	SANE_Status status;
	unsigned char params[1];
	unsigned char *buf;
	size_t buflen;

	DBG(8, "%s\n", __func__);

	if (s->hw->cmd->request_status == 0)
		return SANE_STATUS_UNSUPPORTED;

	buflen = mc_create_buffer (s, s->hw->cmd->scanner_cmd, s->hw->cmd->request_error,
				   &buf, NULL, 1, &status);
	if (buflen <= 0 ) {
		return SANE_STATUS_NO_MEM;
	} else if (status != SANE_STATUS_GOOD) {
		return status;
	}

	status = mc_txrx (s, buf, buflen, params, 1);
	free(buf);
	if (status != SANE_STATUS_GOOD)
		return status;

	DBG(1, "status: %02x\n", params[0]);

	switch (params[0]) {
		case STATUS_READY:
			DBG(1, " ready\n");
			break;
		case STATUS_ADF_JAM:
			DBG(1, " paper jam in ADF\n");
			return SANE_STATUS_JAMMED;
			break;
		case STATUS_OPEN:
			DBG(1, " printer door open or waiting for button press\n");
			return SANE_STATUS_COVER_OPEN;
			break;
		case STATUS_NOT_READY:
			DBG(1, " scanner not ready (in use on another interface or warming up)\n");
			return SANE_STATUS_DEVICE_BUSY;
			break;
		default:
			DBG(1, " unknown status 0x%x\n", params[0]);
	}
	return status;
}

SANE_Status
cmd_request_status(SANE_Handle handle, unsigned char *b)
{
	Magicolor_Scanner *s = (Magicolor_Scanner *) handle;
	SANE_Status status;
	unsigned char *buf;
	size_t buflen;

	DBG(8, "%s\n", __func__);
	if (!b) {
		DBG(1, "%s called with NULL buffer\n", __func__);
		return SANE_STATUS_INVAL;
	}
	memset (b, 0x00, 0x0b); /* initialize all 0x0b bytes with 0 */
	buflen = mc_create_buffer (s, s->hw->cmd->scanner_cmd, s->hw->cmd->request_status,
				   &buf, NULL, 0x0b, &status);
	if (buflen <= 0 ) {
		return SANE_STATUS_NO_MEM;
	} else if (status != SANE_STATUS_GOOD) {
		return status;
	}

	status = mc_txrx (s, buf, buflen, b, 0x0b);
	free (buf);
	if (status != SANE_STATUS_GOOD)
		DBG(8, "%s: Status NOT successfully retrieved\n", __func__);
	else {
		DBG(8, "%s: Status successfully retrieved:\n", __func__);
		/* TODO: debug output of the returned parameters... */
		DBG (11, "  ADF status: 0x%02x", b[1]);
		if (b[1] & ADF_LOADED) {
			DBG (11, " loaded\n");
		} else {
			DBG (11, " not loaded\n");
		}
	}
	return status;
}


SANE_Status
cmd_start_scan (SANE_Handle handle, size_t value)
{
	/* 0x03 08 command*/
	/* TODO: meaning of argument */
	Magicolor_Scanner *s = (Magicolor_Scanner *) handle;
	SANE_Status status;
	unsigned char params1[4], params2[1];
	unsigned char *buf;
	void *p1;
	size_t buflen;

	DBG(8, "%s\n", __func__);
	/* Copy params to buffers */
	/* arg1 is something like byte per line or so.... (TODO) */
	/* arg2 is unknown, seems to be always 0x00 */
	p1 = &params1[0];
	htole32a(p1, value);
	params2[0] = 0x00;
	buflen = mc_create_buffer2 (s, s->hw->cmd->scanner_cmd, s->hw->cmd->start_scanning,
				    &buf, &params1[0], 4, &params2[0], 1, &status);
	if (buflen <= 0 ) {
		return SANE_STATUS_NO_MEM;
	} else if (status != SANE_STATUS_GOOD) {
		return status;
	}

	mc_send(s, buf, buflen, &status);
	free (buf);
	if (status != SANE_STATUS_GOOD)
		DBG(8, "%s: Data NOT successfully sent\n", __func__);
	else
		DBG(8, "%s: Data successfully sent\n", __func__);
	return status;
}

SANE_Status
cmd_cancel_scan (SANE_Handle handle)
{
	/* 0x03 0a command*/
	/* TODO: Does this command really mean CANCEL??? */
	Magicolor_Scanner *s = (Magicolor_Scanner *) handle;
	SANE_Status status;
	unsigned char *buf;
	size_t buflen;

	DBG(8, "%s\n", __func__);
	buflen = mc_create_buffer (s, s->hw->cmd->scanner_cmd, s->hw->cmd->stop_scanning,
				    &buf, NULL, 0, &status);
	if (buflen <= 0 ) {
		return SANE_STATUS_NO_MEM;
	} else if (status != SANE_STATUS_GOOD) {
		return status;
	}

	mc_send(s, buf, buflen, &status);
	free (buf);
	if (status != SANE_STATUS_GOOD)
		DBG(8, "%s: Data NOT successfully sent\n", __func__);
	else
		DBG(8, "%s: Data successfully sent\n", __func__);
	return status;
}

SANE_Status
cmd_finish_scan (SANE_Handle handle)
{
	/* 0x03 12 command*/
	/* TODO: Does this command really mean FINISH??? */
	Magicolor_Scanner *s = (Magicolor_Scanner *) handle;
	SANE_Status status;
	unsigned char *buf, returned[0x0b];
	size_t buflen;

	DBG(8, "%s\n", __func__);
	buflen = mc_create_buffer (s, s->hw->cmd->scanner_cmd, s->hw->cmd->unknown2,
				    &buf, NULL, 0x0b, &status);
	if (buflen <= 0 ) {
		return SANE_STATUS_NO_MEM;
	} else if (status != SANE_STATUS_GOOD) {
		return status;
	}
	memset (&returned[0], 0x00, 0x0b);

	status = mc_txrx (s, buf, buflen, returned, 0x0b);
	free (buf);
	if (status != SANE_STATUS_GOOD)
		DBG(8, "%s: Data NOT successfully sent\n", __func__);
	else
		DBG(8, "%s: Data successfully sent\n", __func__);
	return status;
}

SANE_Status
cmd_get_scanning_parameters(SANE_Handle handle,
			    SANE_Frame *format, SANE_Int *depth,
			    SANE_Int *data_pixels, SANE_Int *pixels_per_line,
			    SANE_Int *lines)
{
	/* 0x03 0b command*/
	Magicolor_Scanner *s = (Magicolor_Scanner *) handle;
	SANE_Status status;
	unsigned char *txbuf, rxbuf[8];
	/* Needed to prevent a compiler warning about type-punned variable: */
	char *b = (char*)rxbuf;
	size_t buflen;
	NOT_USED (format);
	NOT_USED (depth);

	DBG(8, "%s\n", __func__);
	buflen = mc_create_buffer (s, s->hw->cmd->scanner_cmd,
				   s->hw->cmd->request_scan_parameters,
				   &txbuf, NULL, 8, &status);
	if (buflen <= 0 ) {
		return SANE_STATUS_NO_MEM;
	} else if (status != SANE_STATUS_GOOD) {
		return status;
	}

	status = mc_txrx (s, txbuf, buflen, rxbuf, 8);
	free (txbuf);
	if (status != SANE_STATUS_GOOD)
		DBG(8, "%s: Parameters NOT successfully retrieved\n", __func__);
	else {
		DBG(8, "%s: Parameters successfully retrieved\n", __func__);

		/* Assign px_per_line and lines. Bytes 7-8 must match 3-4 */
		if (rxbuf[2]!=rxbuf[6] || rxbuf[3]!=rxbuf[7]) {
			int i;
			DBG (1, "%s: ERROR: Returned image parameters indicate an "
				"unsupported device: Bytes 3-4 do not match "
				"bytes 7-8! Trying to continue with bytes 3-4.\n",
				__func__);
			dump_hex_buffer_dense (1, rxbuf, 8);
		}
		*data_pixels = le16atoh (&b[0]);
		*lines = le16atoh (&b[2]);
		*pixels_per_line = le16atoh (&b[4]);
		/* TODO: debug output of the returned parameters... */
	}

	return status;
}

SANE_Status
cmd_set_scanning_parameters(SANE_Handle handle,
	unsigned char resolution, unsigned char color_mode,
	unsigned char brightness, unsigned char contrast,
	int tl_x, int tl_y, int width, int height, unsigned char source)
{
	/* 0x03 0c command*/
	Magicolor_Scanner *s = (Magicolor_Scanner *) handle;
	SANE_Status status;
	unsigned char param[0x11];
	unsigned char *buf;
	size_t buflen;

	DBG(8, "%s\n", __func__);
	/* Copy over the params to the param byte array */
	/* Byte structure:
	 *   byte 0:     resolution
	 *   byte 1:     color mode
	 *   byte 2:     brightness
	 *   byte 3:     0xff
	 *   byte 4-5:   x-start
	 *   byte 6-7:   y-start
	 *   byte 8-9:  x-extent
	 *   byte 10-11: y-extent
	 *   byte 12:    source (ADF/FBF)
	 **/
	memset (&param[0], 0x00, 0x11);
	param[0] = resolution;
	param[1] = color_mode;
	param[2] = brightness;
	param[3] = contrast | 0xff; /* TODO: Always 0xff? What about contrast? */
	htole16a (&param[4], tl_x);
	htole16a (&param[6], tl_y);
	htole16a (&param[8], width);
	htole16a (&param[10], height);
	param[12] = source;

	/* dump buffer if appropriate */
	DBG (127, "  Scanning parameter buffer:");
	dump_hex_buffer_dense (127, param, 0x11);

	buflen = mc_create_buffer (s, s->hw->cmd->scanner_cmd, s->hw->cmd->set_scan_parameters,
				   &buf, param, 0x11, &status);
	if (buflen <= 0 ) {
		return SANE_STATUS_NO_MEM;
	} else if (status != SANE_STATUS_GOOD) {
		return status;
	}

	mc_send(s, buf, buflen, &status);
	free (buf);
	if (status != SANE_STATUS_GOOD)
		DBG(8, "%s: Data NOT successfully sent\n", __func__);
	else
		DBG(8, "%s: Data successfully sent\n", __func__);
	return status;
}


SANE_Status
cmd_request_push_button_status(SANE_Handle handle, unsigned char *bstatus)
{
	Magicolor_Scanner *s = (Magicolor_Scanner *) handle;
	SANE_Status status;
	unsigned char *buf;
	size_t buflen;

	DBG(8, "%s\n", __func__);


	if (s->hw->cmd->unknown1 == 0)
		return SANE_STATUS_UNSUPPORTED;

	DBG(8, "%s: Supported\n", __func__);
	memset (bstatus, 0x00, 1);
	buflen = mc_create_buffer (s, s->hw->cmd->scanner_cmd, s->hw->cmd->unknown1,
				   &buf, bstatus, 1, &status);
	if (buflen <= 0 ) {
		return SANE_STATUS_NO_MEM;
	} else if (status != SANE_STATUS_GOOD) {
		return status;
	}

	status = mc_txrx (s, buf, buflen, bstatus, 1);
	free(buf);
	if (status != SANE_STATUS_GOOD)
		return status;

	DBG(1, "push button status: %02x ", bstatus[0]);
	switch (bstatus[0]) {
		/* TODO: What's the response code for button pressed??? */
		default:
			DBG(1, " unknown\n");
			status = SANE_STATUS_UNSUPPORTED;
	}
	return status;
}

SANE_Status
cmd_read_data (SANE_Handle handle, unsigned char *buf, size_t len)
{
	Magicolor_Scanner *s = (Magicolor_Scanner *) handle;
	SANE_Status status;
	unsigned char *txbuf;
	unsigned char param[4];
	void *tmp;
	size_t txbuflen;

	DBG(8, "%s\n", __func__);
	tmp = &param[0];
	htole32a(tmp, len);

	txbuflen = mc_create_buffer (s, s->hw->cmd->scanner_cmd, s->hw->cmd->request_data,
				   &txbuf, param, 4, &status);
	if (txbuflen <= 0 ) {
		return SANE_STATUS_NO_MEM;
	} else if (status != SANE_STATUS_GOOD) {
		return status;
	}

	status = mc_txrx (s, txbuf, txbuflen, buf, len);
	free (txbuf);
	if (status != SANE_STATUS_GOOD)
		DBG(8, "%s: Image data NOT successfully retrieved\n", __func__);
	else {
		DBG(8, "%s: Image data successfully retrieved\n", __func__);
	}

	return status;
}