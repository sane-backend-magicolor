/*
 * I/O routines for Magicolor scanners
 *
 * (C) 2010 Reinhold Kainhofer <reinhold@kainhofer.com>
 *
 * Based on the epson2 sane backend:
 * Based on Kazuhiro Sasayama previous
 * Work on epson.[ch] file from the SANE package.
 * Please see those files for original copyrights.
 * Copyright (C) 2006 Tower Technologies
 * Author: Alessandro Zummo <a.zummo@towertech.it>
 *
 * This file is part of the SANE package.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, version 2.
 */

#define DEBUG_DECLARE_ONLY

#include "sane/config.h"

#include <ctype.h>

#include "magicolor.h"
#include "magicolor-io.h"

#include "sane/sanei_usb.h"
#include "sane/sanei_tcp.h"

#include "magicolor_usb.h"
#include "magicolor_net.h"

#include "byteorder.h"

#define min(x,y) (((x)<(y))?(x):(y))


void dump_hex_buffer_dense (int level, const unsigned char *buf, size_t buf_size)
{
	size_t k;
	char msg[1024], fmt_buf[1024];
	memset (&msg[0], 0x00, 1024);
	memset (&fmt_buf[0], 0x00, 1024);
	for (k = 0; k < min(buf_size, 80); k++) {
		if (k % 16 == 0) {
			if (k>0) {
				DBG (level, "%s\n", msg);
				memset (&msg[0], 0x00, 1024);
			}
			sprintf (fmt_buf, "     0x%04x  ", k);
			strcat (msg, fmt_buf);
		}
		if (k % 8 == 0) {
			strcat (msg, " ");
		}
		sprintf (fmt_buf, " %02x" , buf[k]);
		strcat (msg, fmt_buf);
	}
	if (msg[0] != 0 ) {
		DBG (level, "%s\n", msg);
	}
}
#if 0
		DBG
		for (k = 2; k < buf_size; k++) {
			DBG(125, "buf[%d] %02x %c\n", k, s[k],
			    isprint(s[k]) ? s[k] : '.');
		}
#endif

int mc_create_buffer (Magicolor_Scanner *s, unsigned char cmd_type, unsigned char cmd,
		      unsigned char **buf, unsigned char* arg1, size_t len1,
		      SANE_Status *status)
{
	unsigned char* b = NULL;
	size_t buf_len = 2+4+len1+4;
	NOT_USED (s);
	if (len1 <= 0)
		buf_len = 6; /* no args, just cmd + final 0x00 00 00 00 */
	*buf = b = malloc (buf_len);
	memset (b, 0x00, buf_len);
	if (!b) {
		*status = SANE_STATUS_NO_MEM;
		return 0;
	}
	b[0] = cmd_type;
	b[1] = cmd;
	if (len1>0) {
		htole32a (&b[2], len1);
		if (arg1)
			memcpy(b+6, arg1, len1);
	}
	/* Writing the final 0x00 00 00 00 is not necessary, they are 0x00 already */
	*status = SANE_STATUS_GOOD;
	return buf_len;
}

int mc_create_buffer2 (Magicolor_Scanner *s, unsigned char cmd_type, unsigned char cmd,
		       unsigned char **buf, unsigned char* arg1, size_t len1,
		       unsigned char* arg2, size_t len2, SANE_Status *status)
{
	unsigned char* b = NULL;
	size_t buf_len = 2+4+len1+4+len2+4;
	/* If any of the two args has size 0, use the simpler mc_create_buffer */
	if (len1<=0)
		return mc_create_buffer (s, cmd_type, cmd, buf, arg2, len2, status);
	else if (len2<=0)
		return mc_create_buffer (s, cmd_type, cmd, buf, arg1, len1, status);
	/* Allocate memory and copy over args and their lengths */
	*buf = b = malloc (buf_len);
	if (!b) {
		*status = SANE_STATUS_NO_MEM;
		return 0;
	}
	memset (b, 0x00, buf_len);
	b[0] = cmd_type;
	b[1] = cmd;
	htole32a(&b[2], len1);
	if (arg1) {
		memcpy(b+6, arg1, len1);
	}
	htole32a(&b[6+len1], len2);
	if (arg2) {
		memcpy(10+buf+len1, arg2, len2);
	}
	*status = SANE_STATUS_GOOD;
	return buf_len;
}

int
mc_send(Magicolor_Scanner * s, void *buf, size_t buf_size, SANE_Status * status)
{
	DBG(15, "%s: size = %lu\n", __func__, (u_long) buf_size);

	if (DBG_LEVEL >= 125) {
		const unsigned char *s = buf;
		DBG(125, "Cmd: 0x%02x %02x, complete buffer:\n", s[0], s[1]);
		dump_hex_buffer_dense (125, s, buf_size);
	}

	if (s->hw->connection == SANE_MAGICOLOR_NET) {
		return sanei_magicolor_net_write(s, buf, buf_size, status);
	} else if (s->hw->connection == SANE_MAGICOLOR_USB) {
		size_t n;
		n = buf_size;
		*status = sanei_usb_write_bulk(s->fd, buf, &n);
		DBG(125, "USB: wrote %d bytes, status: %s\n", n, sane_strstatus(*status));
		return n;
	}

	*status = SANE_STATUS_INVAL;
	return 0;
	/* never reached */
}

ssize_t
mc_recv(Magicolor_Scanner * s, void *buf, ssize_t buf_size,
	    SANE_Status * status)
{
	ssize_t n = 0;

	DBG(15, "%s: size = %ld, buf = %p\n", __func__, (long) buf_size, buf);

	if (s->hw->connection == SANE_MAGICOLOR_NET) {
		n = sanei_magicolor_net_read(s, buf, buf_size, status);
	} else if (s->hw->connection == SANE_MAGICOLOR_USB) {
		/* !!! only report an error if we don't read anything */
		n = buf_size;	/* buf_size gets overwritten */
		*status =
			sanei_usb_read_bulk(s->fd, (SANE_Byte *) buf,
					    (size_t *) & n);

		if (n > 0)
			*status = SANE_STATUS_GOOD;
	}

	if (n < buf_size) {
		DBG(1, "%s: expected = %lu, got = %ld\n", __func__,
		    (u_long) buf_size, (long) n);
		*status = SANE_STATUS_IO_ERROR;
	}

	/* dump buffer if appropriate */
	if (DBG_LEVEL >= 127 && n > 0) {
		const unsigned char* b=buf;
		dump_hex_buffer_dense (125, b, buf_size);
	}

	return n;
}

/* Simple function to exchange a fixed amount of
 * data with the scanner
 */
SANE_Status
mc_txrx(Magicolor_Scanner * s, unsigned char *txbuf, size_t txlen,
	    unsigned char *rxbuf, size_t rxlen)
{
	SANE_Status status;

	mc_send(s, txbuf, txlen, &status);
	if (status != SANE_STATUS_GOOD) {
		DBG(1, "%s: tx err, %s\n", __func__, sane_strstatus(status));
		return status;
	}

	mc_recv(s, rxbuf, rxlen, &status);
	if (status != SANE_STATUS_GOOD) {
		DBG(1, "%s: rx err, %s\n", __func__, sane_strstatus(status));
	}

	return status;
}

