#!/usr/bin/make -f
# Sample debian/rules that uses debhelper.
# GNU copyright 1997 to 1999 by Joey Hess.

# Uncomment this to turn on verbose mode.
#export DH_VERBOSE=1

# These are used for cross-compiling and for saving the configure script
# from having to guess our platform (since we know it already)
DEB_HOST_GNU_TYPE   ?= $(shell dpkg-architecture -qDEB_HOST_GNU_TYPE)
DEB_BUILD_GNU_TYPE  ?= $(shell dpkg-architecture -qDEB_BUILD_GNU_TYPE)
DEB_HOST_ARCH_OS ?= $(shell dpkg-architecture -qDEB_HOST_ARCH_OS)
ifeq ($(DEB_HOST_ARCH_OS),)
  DEB_HOST_ARCH_OS := $(subst -gnu,,$(shell dpkg-architecture -qDEB_HOST_GNU_SYSTEM))
  ifeq ($(DEB_HOST_ARCH_OS),gnu)
    DEB_HOST_ARCH_OS := hurd
  endif
endif


ifneq (,$(findstring debug,$(DEB_BUILD_OPTIONS)))
	CFLAGS += -g
endif
ifeq (,$(findstring nostrip,$(DEB_BUILD_OPTIONS)))
	INSTALL_PROGRAM += -s
endif

autotools: autotools-stamp
autotools-stamp:
	-rm -f config.sub config.guess
	cp /usr/share/misc/config.sub config.sub
	cp /usr/share/misc/config.guess config.guess
	touch autotools-stamp

config.status: autotools-stamp configure
	dh_testdir
	# Add here commands to configure the package.
	./configure --host=$(DEB_HOST_GNU_TYPE) --build=$(DEB_BUILD_GNU_TYPE) \
		--prefix=/usr --sysconfdir=/etc \
		--datadir=\$${prefix}/share \
		--mandir=\$${prefix}/share/man \
		--localstatedir=/var \
		--disable-locking \
		--enable-static \
		--disable-fork-process \
		--without-gphoto2

build: build-stamp
build-stamp: config.status
	dh_testdir

	# Add here commands to compile the package.
	$(MAKE)

	touch build-stamp

clean:
	dh_testdir
	dh_testroot
	rm -f autotools-stamp build-stamp

	# Add here commands to clean up after the build process.
	[ ! -f Makefile ] || $(MAKE) distclean

	rm -f debian/libsane-extras.dirs

	dh_clean

install: build
	dh_testdir
	dh_testroot
	dh_clean -k
	dh_installdirs

	# Add here commands to install the package into debian/tmp
	$(MAKE) install DESTDIR=$(CURDIR)/debian/tmp

# Build architecture-independent files here.
binary-indep: build install
# We have nothing to do by default.

# Build architecture-dependent files here.
binary-arch: build install
	dh_testdir
	dh_testroot
	dh_install

ifeq (kfreebsd,$(DEB_HOST_ARCH_OS))
	rm -f debian/libsane-extras/usr/share/man/man5/sane-magicolor
	rm -rf debian/libsane-extras/usr/share/doc/libsane-extras/magicolor
else
	dh_installudev
endif

	# Install HAL fdi file
	#mkdir -p $(CURDIR)/debian/libsane-extras/usr/share/hal/fdi/preprobe/10osvendor
	#cp $(CURDIR)/debian/libsane-extras.fdi $(CURDIR)/debian/libsane-extras/usr/share/hal/fdi/preprobe/10osvendor/20-libsane-extras.fdi

	# remove wrong libsane.so.1 symlink
	rm -f debian/libsane-extras/usr/lib/sane/libsane.so.1

	# install the dll.d config file
	install -d debian/libsane-extras/etc/sane.d/dll.d
	cp debian/dll.d/libsane-extras debian/libsane-extras/etc/sane.d/dll.d

#	dh_installdebconf
	dh_installdocs
	dh_installman
#	dh_installinfo
#	dh_undocumented
	dh_installchangelogs
	dh_link
	dh_strip -plibsane-extras --dbg-package=libsane-extras-dbg
	dh_strip
	dh_compress
	dh_fixperms
#	dh_makeshlibs
	dh_installdeb
	dh_shlibdeps
	dh_gencontrol
	dh_md5sums
	dh_builddeb

binary: binary-indep binary-arch
.PHONY: build clean binary-indep binary-arch binary install autotools
