.\" .IX sane-geniusvp2
.TH "sane-geniusvp2" "5" "08 Feb 2005" "@PACKAGEVERSION@" "SANE Scanner Access Now Easy"
.SH "NAME"
sane\-geniusvp2 \- SANE backend for Genius ColorPage\-Vivid Pro II (PP) scanner
.SH "DESCRIPTION"
The
.B sane\-geniusvp2
backend supports EICI 091000 based parallel port flatbed scanners. This ASIC can be found on some Primax devices, and on the Genius ColorPage\-Vivid Pro II scanner (which is actually a Primax OEM device).
.SH "SUPPORTED DEVICES"
The following scanner should work with this backend:

Device Details
.br 
\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-
.br 
Vendor: Genius
.br 
Model: ColorPage\-Vivid Pro II
.br 
FCC ID: EMJFB308C
.br 
MODEL NO: FB601C

Chipset Details
.br 
\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-
.br 
ASIC ID: 0xE1
.br 
Product ID: 0x06
.br 
ASIC: EICI 091000
.br 
AFE: Wolfson WM\-8143\-12
.br 
Memory: NEC uPD424210\-60 (256 Kb?)
.SH "CONFIGURATION"
.PP 
This section decribes the backend's configuration file entries. The file is located at:
.IP 
.I @CONFIGDIR@/geniusvp2.conf
.PP 
For a proper setup, you will need at least this entry:
.IP 
.I device parport0
.PP 
Currently, only access through ppdev is supported.
.SH "PARALLEL PORT MODES"
.PP 
Only EPP mode (including "EPP+ECP") is supported. Future versions may include support for the (slower) SPP mode.
.SH "FILES"
.TP 
.I @CONFIGDIR@/geniusvp2.conf
The backend configuration file
.TP 
.I @LIBDIR@/libsane\-geniusvp2.a
The static library implementing this backend.
.TP 
.I @LIBDIR@/libsane\-geniusvp2.so
The shared library implementing this backend (present on systems that support dynamic loading).
.SH "ENVIRONMENT"
.TP 
.B SANE_CONFIG_DIR
This environment variable specifies the list of directories that may contain the configuration file.  Under UNIX, the directories are separated by a colon (`:'), under OS/2, they are separated by a semi\-colon (`;').  If this variable is not set, the configuration file is searched in two default directories: first, the current working directory (".") and then in @CONFIGDIR@.  If the value of the environment variable ends with the directory separator character, then the default directories are searched after the explicitly specified directories.  For example, setting
.B SANE_CONFIG_DIR
to "/tmp/config:" would result in directories "/tmp/config", ".", and "@CONFIGDIR@" being searched (in this order).
.TP 
.B SANE_DEBUG_GENIUSVP2
If the library was compiled with debug support enabled, this environment variable controls the debug level for this backend.  Higher debug levels increase the verbosity of the output.

Example: export SANE_DEBUG_GENIUSVP2=10

To obtain debug messages from the backend, set this environment variable before calling your favorite frontend (e.g. xscanimage).

Example: export SANE_DEBUG_GENIUSVP2=10 ; xscanimage
.SH "KNOWN BUGS AND RESTRICTIONS"
.PP 
Scan may sometimes timeout due to the scanner's buffer not being filled after some reasonable time, and you get a truncated image. Just try scanning again and it should work.
.PP 
Scans at high resolutions (e.g. 600 dpi.) may also be truncated if the selected scan area is too large. Just scan at lower (<= 300 dpi.) resolutions for now if you want a larger image.
.PP 
The lamp carriage may sometimes go out of its maximum distance (297 mm), forcing the motor engine. You can detect this when the scanner produces a "strange" noise and the carriage is at its maximum distance from home. In this situation,
.B turn off scanner power imediately,
or you may risk damage your device. This seems to happen more often when scanning full pages at high resolutions.
.SH "SEE ALSO"
.BR sane (7),
.br 
.I http://sourceforge.net/projects/geniusvp2/
.SH "AUTHOR"
.PP 
Anderson Lizardo <lizardo@users.sourceforge.net>
